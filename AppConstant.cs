﻿using System;
namespace AMS {

    class Server {
        public const string path = "http://fyp.billchung.net/";
        public const string file = "UploadFolder/";
        public const string filefolder = "wwwroot/UploadFolder/";
    }

    class ReturnStatus {
        public const string success = "Success";
        public const string fail = "Fail";
    }

    class ReturnMessage {
        public const string AccNotFound = "Account Not Found";
        public const string fail = "Fail";
    }

    class AttendanceStatus {
        public const string NoRecord = "No Record";
        public const string late = "Late";
        public const string OnDuty = "On Duty";
        public const string OffWork = "Out of Office";
        public const string created = "Created";
        public const string accepted = "Accepted";
        public const string rejected = "Rejected";
		public const string OT = "OT";
		public const string WorkingButAbsenceRecordExist = "On Working But Absence Record Exist";
    }

    class AbsenceStatus{
        public const string created = "Created";
        public const string approved = "Approved";
        public const string rejected = "Rejected";       
    }

    class StaffAccountStatus {
        public const string Effective = "Effective";
        public const string Ineffective = "Ineffective";
    }

    class StaffType {
        public const string Staff = "Staff";
        public const string DepartmentManager = "DepartmentManager";
        public const string HRStaff = "HRStaff";
        public const string ITAdmin = "ITAdmin";
    }

	class OfficeTime {
		public const int dutyHour = 9;
		public const int dutyMin = 0;
		public const int offHour = 17;
        public const int offMin = 30;
        public const int bufferMin = 30;
		public const int lunchHour = 1;
	}

	class common {
		public const string InsertRecordLocationByHead = "Insert By Admin";
		public const string DateFormat = "dd-MM-yy";
		public const string DateTimeFormat = "dd-MM-yy HH:mm";
        public const string TimeFormat = "HH:mm";
	}

}
