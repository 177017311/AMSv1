﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using ClosedXML.Excel;
using System.IO;

namespace AMS {
    public class ClosedXMLHelper {
        XLWorkbook _wb = new XLWorkbook();


        public string ExportToExcel(DataTable dataTable, string exportFilePath, string exportFileName, string excelTitle, string excelCategory, string tag) {
            string exportedFilePath = exportFilePath + exportFileName + ".xlsx";

            CreateExcel(dataTable, exportFileName, excelTitle, excelCategory, tag);
            _wb.SaveAs(exportedFilePath);

            return exportedFilePath;
        }


        public string ExportToExcel(List<DataTable> dataTables, string exportFilePath, string exportFileName, string excelTitle, string excelCategory, string tag) {
            string exportedFilePath = exportFilePath + exportFileName + ".xlsx";

            CreateExcel(dataTables, exportFileName, excelTitle, excelCategory, tag);
            _wb.SaveAs(exportedFilePath);

            return exportedFilePath;
        }

        public MemoryStream ExportToExcel(DataTable dataTable, string exportFileName, string excelTitle, string excelCategory, string tag) {
            MemoryStream fs = new MemoryStream();

            CreateExcel(dataTable, exportFileName, excelTitle, excelCategory, tag);
            _wb.SaveAs(fs);

            return fs;
        }

        public MemoryStream ExportToExcel(List<DataTable> dataTables, string exportFileName, string excelTitle, string excelCategory, string tag) {
            MemoryStream fs = new MemoryStream();

            CreateExcel(dataTables, exportFileName, excelTitle, excelCategory, tag);
            _wb.SaveAs(fs);

            return fs;
        }


        public void CreateExcel(DataTable dataTable, string exportFileName, string excelTitle, string excelCategory, string tag) {
            _wb.Properties.Title = excelTitle;
            _wb.Properties.Category = excelCategory;
            _wb.Properties.Subject = tag;

            CreateWorkBook(dataTable);
        }

        public void CreateExcel(List<DataTable> dataTables, string exportFileName, string excelTitle, string excelCategory, string tag) {
            _wb.Properties.Title = excelTitle;
            _wb.Properties.Category = excelCategory;
            _wb.Properties.Subject = tag;

            foreach (DataTable dataTable in dataTables) {
                CreateWorkBook(dataTable);
            }
        }

        private void CreateWorkBook(DataTable dataTable) {
            List<DataRow> data = new List<DataRow>();
            foreach (DataRow row in dataTable.Rows) {
                data.Add(row);
            }

            IXLWorksheet ws = _wb.Worksheets.Add(dataTable.TableName);
            IXLTable tableWithData = ws.Cell(1, 1).InsertTable(data);
            ws.Columns().AdjustToContents();
        }

    }
}

