﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AMS.Models;
using System.Linq;
using AMS.Data;
using AMS.Models.AccountViewModels;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;
using System;

namespace AMS.Controllers
{
    [Route("api/[controller]")]
    public class AbsenceRecordAPIController : APIController
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public AbsenceRecordAPIController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, ApplicationDbContext context) {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }


        // POST api/values
        [HttpPost]
        public string Post([FromBody]AbsenceRecordAPIViewModel model)
        {

            if (ModelState.IsValid) {

                ApplicationUser user = _context.Users.Where(o => o.Id == model.Id).FirstOrDefault();
                if (user == null) {
                    return returnNoVaildJson(ReturnMessage.AccNotFound);
                }

                var imageBase64 = model.image;
                var fileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + ".jpg";
                if (!string.IsNullOrWhiteSpace(imageBase64)) {
                    var reg = new Regex("data:image/(.*);base64,");
                    imageBase64 = reg.Replace(imageBase64, "");
                    byte[] imageByte = Convert.FromBase64String(imageBase64);
                    var stream = new MemoryStream(imageByte);

                    string _folder = Server.filefolder;

                    var path = Path.Combine(_folder,fileName);
                    using (FileStream file = new FileStream(path, FileMode.Create, System.IO.FileAccess.Write)) {
                        byte[] bytes = new byte[stream.Length];
                        stream.Read(bytes, 0, (int)stream.Length);
                        file.Write(bytes, 0, bytes.Length);
                        stream.Close();
                    }
                }

                AbsenceRecord absenceRecord = new AbsenceRecord();
                absenceRecord.ApplicationUser = user;
                absenceRecord.LeaveType = model.leaveType;
                absenceRecord.LeaveStartDate = model.leaveStartDate;
                absenceRecord.LeaveEndDate = model.leaveEndDate;
                absenceRecord.Remark = model.remark;
                absenceRecord.image = string.IsNullOrEmpty(model.image) ? string.Empty : fileName;
                absenceRecord.Status = AbsenceStatus.created;

                if (ModelState.IsValid) {
                    _context.Add(absenceRecord);
                    _context.SaveChanges();
                }

                return returnSuccessJson("Record Created");

            } else {
                return returnNoVaildJson(ModelState.Values.First().Errors.First().ErrorMessage);
            }


        }


    }
}