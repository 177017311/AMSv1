﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AMS.Models;
using System.Linq;
using AMS.Data;
using AMS.Models.AccountViewModels;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;


namespace AMS.Controllers
{
    [Route("api/[controller]")]
    public class AttendanceRecordAPIController : APIController
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public AttendanceRecordAPIController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, ApplicationDbContext context) {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }


        // POST api/values
        [HttpPost]
        public string Post([FromBody]AttendanceAPIViewModel model) {
            if (ModelState.IsValid) {

                ApplicationUser user = _context.Users.Where(o => o.Id == model.Id).FirstOrDefault();
                if (user == null) {
                    return returnNoVaildJson(ReturnMessage.AccNotFound);
                }

                HotspotLocation hotspotLocation = _context.HotspotLocation.Where(o => o.iBeaconLocationMajor == model.iBeaconLocationMajor.PadLeft(4, '0')
                                                                                 && o.iBeaconLocationMinor == model.iBeaconLocationMinor.PadLeft(4, '0')).SingleOrDefault();
                if (hotspotLocation == null) {
                    return returnNoVaildJson("Not valid hotspot");
                }


                AttendanceRecord attendanceRecord = new AttendanceRecord();
                attendanceRecord.ApplicationUser = user;
                attendanceRecord.HotspotLocation = hotspotLocation;
                attendanceRecord.RecordTimeStamp = model.attendanceTimeStamp;
                attendanceRecord.Location = hotspotLocation.LocationDescription;
                attendanceRecord.Status = AttendanceStatus.created;

                if (ModelState.IsValid) {
                    _context.Add(attendanceRecord);
                    _context.SaveChanges();
                }

                string returnJson = Newtonsoft.Json.JsonConvert.SerializeObject(new {
                    status = ReturnStatus.success,
                    attendanceID = model.attendanceID,
                    message = string.Empty
                });

                return returnJson;

            } else {
                return returnNoVaildJson(ModelState.Values.First().Errors.First().ErrorMessage);
            }
        }

    }
}