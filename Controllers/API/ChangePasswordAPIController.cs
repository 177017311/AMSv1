﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AMS.Models;
using System.Linq;
using AMS.Data;
using AMS.Models.AccountViewModels;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace AMS.Controllers {
    [Route("api/[controller]")]
    public class ChangePasswordAPIController : APIController {

        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public ChangePasswordAPIController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, ApplicationDbContext context) {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }


        // POST api/values
        [HttpPost]
        public async Task<string> Post([FromBody]ChangePasswordAPIViewModel model) {
            if (ModelState.IsValid) {

                ApplicationUser user = _context.Users.Where(o => o.Id == model.Id).FirstOrDefault();
                if (user == null) {
                    return returnNoVaildJson(ReturnMessage.AccNotFound);
                }

                var changePasswordResult = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
                if (!changePasswordResult.Succeeded) {
                    return returnNoVaildJson(changePasswordResult.Errors.First().Description);
                }

                return returnSuccessJson("Password Changed");

            } else {
                return returnNoVaildJson(ModelState.Values.First().Errors.First().ErrorMessage);
            }
        }
    }
}