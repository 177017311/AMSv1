﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using AMS.Models;
using AMS.Models.AccountViewModels;
using AMS.Services;
using AMS.Data;

namespace AMS.Controllers {
    [Route("api/[controller]")]
    public class DataLoadingAPIController : APIController {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

		public DataLoadingAPIController(UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            ILogger<AccountController> logger,
            ApplicationDbContext context) {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
            _context = context;
        }



        // POST api/values
        [HttpGet]
		public async Task<string>  Get() {
            List<Organization> organizations = new List<Organization>();
            Organization organization = new Organization();
            //organization.Division = "IT";
            //organization.Department = "SD";
            //organization.Section = "";
            //organizations.Add(organization);

            //organization = new Organization();
            organization.Division = "IT";
            organization.Department = "INT";
            organization.Section = "";
            organizations.Add(organization);
 
            //organization = new Organization();
            //organization.Division = "HR";
            //organization.Department = "AD";
            //organization.Section = "";
            //organizations.Add(organization);

            //organization = new Organization();
            //organization.Division = "CS";
            //organization.Department = "CS";
            //organization.Section = "";
            //organizations.Add(organization);


            foreach (var org in organizations){
                if (ModelState.IsValid) {
                    _context.Add(org);
                    _context.SaveChanges();
                }                
            }

			List<HotspotLocation> hotspotLocations = new List<HotspotLocation>();
            HotspotLocation hotspotLocation = new HotspotLocation();
   //         hotspotLocation.LocationCode = "GFCB";
   //         hotspotLocation.LocationDescription = "G/F Center Building";
   //         hotspotLocation.iBeaconLocationMajor = "1001";
   //         hotspotLocation.iBeaconLocationMinor = "1002";
			//hotspotLocations.Add(hotspotLocation);

			//hotspotLocation = new HotspotLocation();
			//hotspotLocation.LocationCode = "3ACB";
   //         hotspotLocation.LocationDescription = "3A Meeting Room";
   //         hotspotLocation.iBeaconLocationMajor = "1001";
   //         hotspotLocation.iBeaconLocationMinor = "1003";
   //         hotspotLocations.Add(hotspotLocation);

			//hotspotLocation = new HotspotLocation();
			//hotspotLocation.LocationCode = "6ACB";
   //         hotspotLocation.LocationDescription = "6A Meeting Room";
   //         hotspotLocation.iBeaconLocationMajor = "1001";
   //         hotspotLocation.iBeaconLocationMinor = "1004";
   //         hotspotLocations.Add(hotspotLocation);

			//hotspotLocation = new HotspotLocation();
   //         hotspotLocation.LocationCode = "6BCB";
   //         hotspotLocation.LocationDescription = "6B Meeting Room";
   //         hotspotLocation.iBeaconLocationMajor = "1001";
   //         hotspotLocation.iBeaconLocationMinor = "1005";
   //         hotspotLocations.Add(hotspotLocation);

			//hotspotLocation = new HotspotLocation();
			//hotspotLocation.LocationCode = "6CCB";
   //         hotspotLocation.LocationDescription = "6C Meeting Room";
   //         hotspotLocation.iBeaconLocationMajor = "1001";
   //         hotspotLocation.iBeaconLocationMinor = "1005";
   //         hotspotLocations.Add(hotspotLocation);

			//hotspotLocation = new HotspotLocation();
			//hotspotLocation.LocationCode = "7ACB";
   //         hotspotLocation.LocationDescription = "7A Meeting Room";
   //         hotspotLocation.iBeaconLocationMajor = "1001";
   //         hotspotLocation.iBeaconLocationMinor = "1004";
   //         hotspotLocations.Add(hotspotLocation);

			//hotspotLocation = new HotspotLocation();
			//hotspotLocation.LocationCode = "7BCB";
   //         hotspotLocation.LocationDescription = "7B Meeting Room";
   //         hotspotLocation.iBeaconLocationMajor = "1001";
   //         hotspotLocation.iBeaconLocationMinor = "1005";
   //         hotspotLocations.Add(hotspotLocation);

			//hotspotLocation = new HotspotLocation();
			//hotspotLocation.LocationCode = "Site01";
			//hotspotLocation.LocationDescription = "Working Site 01";
   //         hotspotLocation.iBeaconLocationMajor = "1090";
   //         hotspotLocation.iBeaconLocationMinor = "1001";
   //         hotspotLocations.Add(hotspotLocation);

			//hotspotLocation = new HotspotLocation();
			//hotspotLocation.LocationCode = "Site02";
			//hotspotLocation.LocationDescription = "Working Site 02";
   //         hotspotLocation.iBeaconLocationMajor = "1090";
   //         hotspotLocation.iBeaconLocationMinor = "1002";
   //         hotspotLocations.Add(hotspotLocation);

			//hotspotLocation = new HotspotLocation();
			//hotspotLocation.LocationCode = "Site03";
			//hotspotLocation.LocationDescription = "Working Site 03";
            //hotspotLocation.iBeaconLocationMajor = "1000";
            //hotspotLocation.iBeaconLocationMinor = "9003";
            //hotspotLocations.Add(hotspotLocation);

			//hotspotLocation = new HotspotLocation();
            hotspotLocation.LocationCode = "ADMIN";
            hotspotLocation.LocationDescription = common.InsertRecordLocationByHead;
            hotspotLocation.iBeaconLocationMajor = "9999";
            hotspotLocation.iBeaconLocationMinor = "9999";
			hotspotLocations.Add(hotspotLocation);

			foreach (var loc in hotspotLocations) {
				if (ModelState.IsValid) {
					_context.Add(loc);
					_context.SaveChanges();
				}
			}

			List<StaffAccountViewModel> staffAccountViewModels = new List<StaffAccountViewModel>();
            StaffAccountViewModel staffAccountViewModel = new StaffAccountViewModel();
            staffAccountViewModel.Email = "admin@admin.com";
            staffAccountViewModel.Name = "admin";
            staffAccountViewModel.Password = "Abcd1234!@";
            staffAccountViewModel.StaffType = StaffType.DepartmentManager;
            staffAccountViewModel.Title = "admin";
            staffAccountViewModel.Department = "INT";
            staffAccountViewModel.Division = "IT";
            staffAccountViewModel.Section = "";
            staffAccountViewModel.EffectiveStart = DateTime.ParseExact("2000-01-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            staffAccountViewModel.EffecticeEnd = DateTime.ParseExact("2099-10-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            organization = _context.Organization.Where(o => o.Division == staffAccountViewModel.Division && o.Department == staffAccountViewModel.Department && o.Section == staffAccountViewModel.Section).SingleOrDefault();
            staffAccountViewModel.organization = organization;
            staffAccountViewModels.Add(staffAccountViewModel);


            //staffAccountViewModel = new StaffAccountViewModel();
            //staffAccountViewModel.Email = "jerry@fyp.com";
            //staffAccountViewModel.Password = "Abcd1234!@";
            //staffAccountViewModel.Name = "Jerry Tse";
            //staffAccountViewModel.StaffType = StaffType.ITAdmin;
            //staffAccountViewModel.Title = "IT Admin";
            //staffAccountViewModel.Department = "SD";
            //staffAccountViewModel.Division = "IT";
            //staffAccountViewModel.Section = "";
            //staffAccountViewModel.EffectiveStart = DateTime.ParseExact("2015-01-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //staffAccountViewModel.EffecticeEnd = DateTime.ParseExact("2020-10-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //organization = _context.Organization.Where(o => o.Division == staffAccountViewModel.Division && o.Department == staffAccountViewModel.Department && o.Section == staffAccountViewModel.Section).SingleOrDefault();
            //staffAccountViewModel.organization = organization;
            //staffAccountViewModels.Add(staffAccountViewModel);

            //staffAccountViewModel = new StaffAccountViewModel();
            //staffAccountViewModel.Email = "avia@fyp.com";
            //staffAccountViewModel.Password = "Abcd1234!@";
            //staffAccountViewModel.Name = "Avia Ng";
            //staffAccountViewModel.StaffType = StaffType.HRStaff;
            //staffAccountViewModel.Title = "HR Staff";
            //staffAccountViewModel.Department = "HR";
            //staffAccountViewModel.Division = "AD";
            //staffAccountViewModel.Section = "";
            //staffAccountViewModel.EffectiveStart = DateTime.ParseExact("2015-01-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //staffAccountViewModel.EffecticeEnd = DateTime.ParseExact("2020-10-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //organization = _context.Organization.Where(o => o.Division == staffAccountViewModel.Division && o.Department == staffAccountViewModel.Department && o.Section == staffAccountViewModel.Section).SingleOrDefault();
            //staffAccountViewModel.organization = organization;
            //staffAccountViewModels.Add(staffAccountViewModel);

            //staffAccountViewModel = new StaffAccountViewModel();
            //staffAccountViewModel.Email = "Hanson@fyp.com";
            //staffAccountViewModel.Password = "Abcd1234!@";
            //staffAccountViewModel.Name = "Hanson Lee";
            //staffAccountViewModel.StaffType = StaffType.Staff;
            //staffAccountViewModel.Title = "System Analysis";
            //staffAccountViewModel.Department = "SD";
            //staffAccountViewModel.Division = "IT";
            //staffAccountViewModel.Section = "";
            //staffAccountViewModel.EffectiveStart = DateTime.ParseExact("2015-01-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //staffAccountViewModel.EffecticeEnd = DateTime.ParseExact("2020-10-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //organization = _context.Organization.Where(o => o.Division == staffAccountViewModel.Division && o.Department == staffAccountViewModel.Department && o.Section == staffAccountViewModel.Section).SingleOrDefault();
            //staffAccountViewModel.organization = organization;
            //staffAccountViewModels.Add(staffAccountViewModel);

            //staffAccountViewModel = new StaffAccountViewModel();
            //staffAccountViewModel.Email = "king@fyp.com";
            //staffAccountViewModel.Password = "Abcd1234!@";
            //staffAccountViewModel.Name = "King Lai";
            //staffAccountViewModel.StaffType = StaffType.Staff;
            //staffAccountViewModel.Title = "IT Developer";
            //staffAccountViewModel.Department = "SD";
            //staffAccountViewModel.Division = "IT";
            //staffAccountViewModel.Section = "";
            //staffAccountViewModel.EffectiveStart = DateTime.ParseExact("2015-01-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //staffAccountViewModel.EffecticeEnd = DateTime.ParseExact("2020-10-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //organization = _context.Organization.Where(o => o.Division == staffAccountViewModel.Division && o.Department == staffAccountViewModel.Department && o.Section == staffAccountViewModel.Section).SingleOrDefault();
            //staffAccountViewModel.organization = organization;
            //staffAccountViewModels.Add(staffAccountViewModel);

            //staffAccountViewModel = new StaffAccountViewModel();
            //staffAccountViewModel.Email = "dennis@fyp.com";
            //staffAccountViewModel.Password = "Abcd1234!@";
            //staffAccountViewModel.Name = "Dennis Chu";
            //staffAccountViewModel.StaffType = StaffType.Staff;
            //staffAccountViewModel.Title = "System Analysis";
            //staffAccountViewModel.Department = "SD";
            //staffAccountViewModel.Division = "IT";
            //staffAccountViewModel.Section = "";
            //staffAccountViewModel.EffectiveStart = DateTime.ParseExact("2015-01-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //staffAccountViewModel.EffecticeEnd = DateTime.ParseExact("2020-10-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //organization = _context.Organization.Where(o => o.Division == staffAccountViewModel.Division && o.Department == staffAccountViewModel.Department && o.Section == staffAccountViewModel.Section).SingleOrDefault();
            //staffAccountViewModel.organization = organization;
            //staffAccountViewModels.Add(staffAccountViewModel);

            //staffAccountViewModel = new StaffAccountViewModel();
            //staffAccountViewModel.Email = "Daniel@fyp.com";
            //staffAccountViewModel.Password = "Abcd1234!@";
            //staffAccountViewModel.Name = "Daniel Liu";
            //staffAccountViewModel.StaffType = StaffType.Staff;
            //staffAccountViewModel.Title = "System Analysis";
            //staffAccountViewModel.Department = "SD";
            //staffAccountViewModel.Division = "IT";
            //staffAccountViewModel.Section = "";
            //staffAccountViewModel.EffectiveStart = DateTime.ParseExact("2015-01-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //staffAccountViewModel.EffecticeEnd = DateTime.ParseExact("2020-10-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //organization = _context.Organization.Where(o => o.Division == staffAccountViewModel.Division && o.Department == staffAccountViewModel.Department && o.Section == staffAccountViewModel.Section).SingleOrDefault();
            //staffAccountViewModel.organization = organization;
            //staffAccountViewModels.Add(staffAccountViewModel);

            //staffAccountViewModel = new StaffAccountViewModel();
            //staffAccountViewModel.Email = "Jacky@fyp.com";
            //staffAccountViewModel.Password = "Abcd1234!@";
            //staffAccountViewModel.Name = "Jacky Lee";
            //staffAccountViewModel.StaffType = StaffType.Staff;
            //staffAccountViewModel.Title = "IT Developer";
            //staffAccountViewModel.Department = "SD";
            //staffAccountViewModel.Division = "IT";
            //staffAccountViewModel.Section = "";
            //staffAccountViewModel.EffectiveStart = DateTime.ParseExact("2015-01-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //staffAccountViewModel.EffecticeEnd = DateTime.ParseExact("2020-10-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //organization = _context.Organization.Where(o => o.Division == staffAccountViewModel.Division && o.Department == staffAccountViewModel.Department && o.Section == staffAccountViewModel.Section).SingleOrDefault();
            //staffAccountViewModel.organization = organization;
            //staffAccountViewModels.Add(staffAccountViewModel);

            //staffAccountViewModel = new StaffAccountViewModel();
            //staffAccountViewModel.Email = "peter@fyp.com";
            //staffAccountViewModel.Password = "Abcd1234!@";
            //staffAccountViewModel.Name = "Peter Chow";
            //staffAccountViewModel.StaffType = StaffType.Staff;
            //staffAccountViewModel.Title = "IT Developer";
            //staffAccountViewModel.Department = "SD";
            //staffAccountViewModel.Division = "IT";
            //staffAccountViewModel.Section = "";
            //staffAccountViewModel.EffectiveStart = DateTime.ParseExact("2015-01-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //staffAccountViewModel.EffecticeEnd = DateTime.ParseExact("2020-10-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //organization = _context.Organization.Where(o => o.Division == staffAccountViewModel.Division && o.Department == staffAccountViewModel.Department && o.Section == staffAccountViewModel.Section).SingleOrDefault();
            //staffAccountViewModel.organization = organization;
            //staffAccountViewModels.Add(staffAccountViewModel);

            //staffAccountViewModel = new StaffAccountViewModel();
            //staffAccountViewModel.Email = "Carry@fyp.com";
            //staffAccountViewModel.Password = "Abcd1234!@";
            //staffAccountViewModel.Name = "Carry Ng";
            //staffAccountViewModel.StaffType = StaffType.Staff;
            //staffAccountViewModel.Title = "IT Developer";
            //staffAccountViewModel.Department = "SD";
            //staffAccountViewModel.Division = "IT";
            //staffAccountViewModel.Section = "";
            //staffAccountViewModel.EffectiveStart = DateTime.ParseExact("2015-01-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //staffAccountViewModel.EffecticeEnd = DateTime.ParseExact("2020-10-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //organization = _context.Organization.Where(o => o.Division == staffAccountViewModel.Division && o.Department == staffAccountViewModel.Department && o.Section == staffAccountViewModel.Section).SingleOrDefault();
            //staffAccountViewModel.organization = organization;
            //staffAccountViewModels.Add(staffAccountViewModel);

            //staffAccountViewModel = new StaffAccountViewModel();
            //staffAccountViewModel.Email = "Ken@fyp.com";
            //staffAccountViewModel.Password = "Abcd1234!@";
            //staffAccountViewModel.Name = "Ken Yeun";
            //staffAccountViewModel.StaffType = StaffType.Staff;
            //staffAccountViewModel.Title = "IT Developer";
            //staffAccountViewModel.Department = "SD";
            //staffAccountViewModel.Division = "IT";
            //staffAccountViewModel.Section = "";
            //staffAccountViewModel.EffectiveStart = DateTime.ParseExact("2015-01-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //staffAccountViewModel.EffecticeEnd = DateTime.ParseExact("2020-10-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //organization = _context.Organization.Where(o => o.Division == staffAccountViewModel.Division && o.Department == staffAccountViewModel.Department && o.Section == staffAccountViewModel.Section).SingleOrDefault();
            //staffAccountViewModel.organization = organization;
            //staffAccountViewModels.Add(staffAccountViewModel);

            //staffAccountViewModel = new StaffAccountViewModel();
            //staffAccountViewModel.Email = "Joe@fyp.com";
            //staffAccountViewModel.Password = "Abcd1234!@";
            //staffAccountViewModel.Name = "Joe Yeun";
            //staffAccountViewModel.StaffType = StaffType.Staff;
            //staffAccountViewModel.Title = "IT Developer";
            //staffAccountViewModel.Department = "SD";
            //staffAccountViewModel.Division = "IT";
            //staffAccountViewModel.Section = "";
            //staffAccountViewModel.EffectiveStart = DateTime.ParseExact("2015-01-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //staffAccountViewModel.EffecticeEnd = DateTime.ParseExact("2020-10-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //organization = _context.Organization.Where(o => o.Division == staffAccountViewModel.Division && o.Department == staffAccountViewModel.Department && o.Section == staffAccountViewModel.Section).SingleOrDefault();
            //staffAccountViewModel.organization = organization;
            //staffAccountViewModels.Add(staffAccountViewModel);

            //staffAccountViewModel = new StaffAccountViewModel();
            //staffAccountViewModel.Email = "john@fyp.com";
            //staffAccountViewModel.Password = "Abcd1234!@";
            //staffAccountViewModel.Name = "John Lau";
            //staffAccountViewModel.StaffType = StaffType.Staff;
            //staffAccountViewModel.Title = "CS Staff";
            //staffAccountViewModel.Department = "CS";
            //staffAccountViewModel.Division = "CS";
            //staffAccountViewModel.Section = "";
            //staffAccountViewModel.EffectiveStart = DateTime.ParseExact("2015-01-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //staffAccountViewModel.EffecticeEnd = DateTime.ParseExact("2020-10-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //organization = _context.Organization.Where(o => o.Division == staffAccountViewModel.Division && o.Department == staffAccountViewModel.Department && o.Section == staffAccountViewModel.Section).SingleOrDefault();
            //staffAccountViewModel.organization = organization;
            //staffAccountViewModels.Add(staffAccountViewModel);

            //staffAccountViewModel = new StaffAccountViewModel();
            //staffAccountViewModel.Email = "Kason@fyp.com";
            //staffAccountViewModel.Password = "Abcd1234!@";
            //staffAccountViewModel.Name = "Kason Wong";
            //staffAccountViewModel.StaffType = StaffType.Staff;
            //staffAccountViewModel.Title = "CS Staff";
            //staffAccountViewModel.Department = "CS";
            //staffAccountViewModel.Division = "CS";
            //staffAccountViewModel.Section = "";
            //staffAccountViewModel.EffectiveStart = DateTime.ParseExact("2015-01-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //staffAccountViewModel.EffecticeEnd = DateTime.ParseExact("2020-10-01", "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
            //organization = _context.Organization.Where(o => o.Division == staffAccountViewModel.Division && o.Department == staffAccountViewModel.Department && o.Section == staffAccountViewModel.Section).SingleOrDefault();
            //staffAccountViewModel.organization = organization;
            //staffAccountViewModels.Add(staffAccountViewModel);

            foreach (StaffAccountViewModel model in staffAccountViewModels) {
                if (ModelState.IsValid) {
                    var user = new ApplicationUser { UserName = model.Email, Email = model.Email, StaffName = model.Name ,Title = model.Title ,StaffType = model.StaffType, EffectiveStart = model.EffectiveStart, EffecticeEnd = model.EffecticeEnd, Organization = model.organization };
                    var result = await _userManager.CreateAsync(user, model.Password);
                    if (result.Succeeded) {
                        _logger.LogInformation("User created a new account with password.");

                        var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                        var callbackUrl = Url.EmailConfirmationLink(user.Id, code, Request.Scheme);
                        await _emailSender.SendEmailConfirmationAsync(model.Email, callbackUrl);
                    }
                    //AddErrors(result);
                }
            }

            //HotspotLocation location = _context.HotspotLocation.Where(o => o.LocationCode == "GFCB").Single();

            //foreach (ApplicationUser user in _context.Users.ToList()){
            //    DateTime dateTime = new DateTime(2016, 1, 1);

            //    while (dateTime.Date != DateTime.Today){
            //        if (dateTime.DayOfWeek != DayOfWeek.Saturday && dateTime.DayOfWeek != DayOfWeek.Sunday) {
            //            AttendanceRecord attendanceRecord = new AttendanceRecord();
            //            attendanceRecord.ApplicationUser = user;
            //            attendanceRecord.RecordTimeStamp = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 09, 00, 00);
            //            attendanceRecord.HotspotLocation = location;
            //            attendanceRecord.Location = location.LocationDescription;
            //            attendanceRecord.Status = AttendanceStatus.created;
            //            _context.Add(attendanceRecord);
            //            _context.SaveChanges();

            //            attendanceRecord = new AttendanceRecord();
            //            attendanceRecord.ApplicationUser = user;
            //            attendanceRecord.RecordTimeStamp = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, 17, 30, 00);
            //            attendanceRecord.HotspotLocation = location;
            //            attendanceRecord.Location = location.LocationDescription;
            //            attendanceRecord.Status = AttendanceStatus.created;
            //            _context.Add(attendanceRecord);
            //            _context.SaveChanges();
            //        }

            //        dateTime = dateTime.AddDays(1);
            //    }
            //}





            string returnJson = Newtonsoft.Json.JsonConvert.SerializeObject(new {
                status = ReturnStatus.success,
                message = "Data Loading"
            });

            return returnJson;
        }


    }
}