﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AMS.Models;
using System.Linq;
using System;
using Microsoft.EntityFrameworkCore;
using AMS.Data;

namespace AMS.Controllers {
    [Route("api/[controller]")]
    public class ListAbsenceRecordAPIController : APIController {
        private readonly ApplicationDbContext _context;

        public ListAbsenceRecordAPIController(ApplicationDbContext context) {
            _context = context;
        }


        // POST api/values
        [HttpPost]
        public string Post([FromBody]AbsenceCriteriaAPIViewModel model) {
            if (ModelState.IsValid) {

                if (model.StartDate > model.EndDate) {
                    return returnNoVaildJson("End Date is not later");
                }

                ApplicationUser user = _context.Users.Where(o => o.Id == model.Id).FirstOrDefault();
                if (user == null) {
                    return returnNoVaildJson(ReturnMessage.AccNotFound);
                }

                List<AbsenceRecord> absenceRecords = _context.AbsenceRecord.Where(o => o.ApplicationUser.Id == model.Id
                                                                                  && o.LeaveStartDate >= model.StartDate
                                                                                  && o.LeaveEndDate <= model.EndDate).ToList();

                List<AbsenceRecordAPIViewModel> absenceRecordList = new List<AbsenceRecordAPIViewModel>();
                foreach (AbsenceRecord AbsenceRecord in absenceRecords) {
                    AbsenceRecordAPIViewModel newAbsenceRecord = new AbsenceRecordAPIViewModel();
                    newAbsenceRecord.absenceID = AbsenceRecord.ID.ToString();
                    newAbsenceRecord.leaveType = AbsenceRecord.LeaveType;
                    newAbsenceRecord.leaveStartDate = AbsenceRecord.LeaveStartDate;
                    newAbsenceRecord.leaveEndDate = AbsenceRecord.LeaveEndDate;
                    newAbsenceRecord.remark = AbsenceRecord.Remark;
                    newAbsenceRecord.approveStatus = AbsenceRecord.Status;
                    newAbsenceRecord.managerRemark = AbsenceRecord.ManagerRemark;
                    newAbsenceRecord.imagePath = Server.path + Server.file + AbsenceRecord.image;
                    absenceRecordList.Add(newAbsenceRecord);
                }


                string returnJson = Newtonsoft.Json.JsonConvert.SerializeObject(new {
                    status = ReturnStatus.success,
                    absenceRecordList
                });

                return returnJson;

            } else {
                return returnNoVaildJson(ModelState.Values.First().Errors.First().ErrorMessage);
            }
        }

    }
}
