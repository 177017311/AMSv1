﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AMS.Models;
using System.Linq;
using System;
using Microsoft.EntityFrameworkCore;
using AMS.Data;

namespace AMS.Controllers
{
    [Route("api/[controller]")]
    public class ListAttendanceRecordAPIController : APIController
    {
        private readonly ApplicationDbContext _context;

        public ListAttendanceRecordAPIController(ApplicationDbContext context)
        {
            _context = context;
        }


        // POST api/values
        [HttpPost]
        public string Post([FromBody]AttendanceCriteriaAPIViewModel model)
        {

            if (ModelState.IsValid) {

                if (model.StartDate > model.EndDate){
                    return returnNoVaildJson("End Date is not later");
                }

                ApplicationUser user = _context.Users.Where(o => o.Id == model.Id).FirstOrDefault();
                if (user == null) {
                    return returnNoVaildJson(ReturnMessage.AccNotFound);
                }

                List<AttendanceRecord> attendanceRecords = _context.AttendanceRecord.Where(o => o.ApplicationUser.Id == model.Id
                                                                                           && o.RecordTimeStamp >= model.StartDate
                                                                                           && o.RecordTimeStamp <= model.EndDate).OrderByDescending(i=>i.RecordTimeStamp)
                                                                   .Include(i => i.ApplicationUser).Include(j => j.HotspotLocation).ToList();

                List<AttendanceRecordAPIViewModel> attendanceRecordList = new List<AttendanceRecordAPIViewModel>();
                foreach (AttendanceRecord attendanceReocrd in attendanceRecords) {
                    AttendanceRecordAPIViewModel newAttendanceReocrd = new AttendanceRecordAPIViewModel();
                    newAttendanceReocrd.attendanceTimeStamp = attendanceReocrd.RecordTimeStamp;
                    newAttendanceReocrd.location = attendanceReocrd.HotspotLocation.LocationDescription;
                    newAttendanceReocrd.attendanceStatus = attendanceReocrd.Status;
                    attendanceRecordList.Add(newAttendanceReocrd);
                }


                string returnJson = Newtonsoft.Json.JsonConvert.SerializeObject(new {
                    status = ReturnStatus.success,
                    attendanceRecordList
                });

                return returnJson;

            } else {
                return returnNoVaildJson(ModelState.Values.First().Errors.First().ErrorMessage);
            }
        }


    }
}
