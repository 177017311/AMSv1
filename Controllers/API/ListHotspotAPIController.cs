﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AMS.Models;
using System.Linq;
using System;
using Microsoft.EntityFrameworkCore;
using AMS.Data;

namespace AMS.Controllers {
    [Route("api/[controller]")]
	public class ListHotspotAPIController : APIController {
        private readonly ApplicationDbContext _context;

		public ListHotspotAPIController(ApplicationDbContext context) {
            _context = context;
        }


        // POST api/values
        [HttpPost]
		public string Post([FromBody]HotspotCriteriaAPIViewModel model) {

            if (ModelState.IsValid) {

				if (model.lastRecordDateTime == null){
					model.lastRecordDateTime = DateTime.Now;
				}

				List<HotspotLocation> hotspotLocations = _context.HotspotLocation.ToList();

				List<HotspotAPIViewModel> hotspotList = new List<HotspotAPIViewModel>();
				foreach (HotspotLocation hotspotLocation in hotspotLocations) {
					HotspotAPIViewModel hotspotAPIViewModel = new HotspotAPIViewModel();
					hotspotAPIViewModel.ID = hotspotLocation.ID;
					hotspotAPIViewModel.iBeaconLocationMajor = hotspotLocation.iBeaconLocationMajor;
					hotspotAPIViewModel.iBeaconLocationMinor = hotspotLocation.iBeaconLocationMinor;
					hotspotAPIViewModel.LocationCode = hotspotLocation.LocationCode;
					hotspotAPIViewModel.LocationDescription = hotspotLocation.LocationDescription;
					hotspotList.Add(hotspotAPIViewModel);
                }


                string returnJson = Newtonsoft.Json.JsonConvert.SerializeObject(new {
                    status = ReturnStatus.success,
					hotspotList
                });

                return returnJson;

            } else {
                return returnNoVaildJson(ModelState.Values.First().Errors.First().ErrorMessage);
            }
        }


    }
}
