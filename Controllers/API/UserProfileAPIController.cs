﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AMS.Models;
using System.Linq;
using AMS.Data;
using AMS.Models.AccountViewModels;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace AMS.Controllers {
    [Route("api/[controller]")]
    public class UserProfileAPIController : APIController {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public UserProfileAPIController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, ApplicationDbContext context) {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }


        // POST api/values
        [HttpPost]
        public async Task<string> Post([FromBody]LoginViewModel model) {
            if (ModelState.IsValid) {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var result = await _signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, lockoutOnFailure: false);
                if (result.Succeeded) {

                    ApplicationUser user = _context.Users.Where(o => o.Email == model.Email).First();
                    string returnJson = Newtonsoft.Json.JsonConvert.SerializeObject(new {
                        status = ReturnStatus.success,
                        Id = user.Id,
                        email = user.Email,
						phoneNo = user.PhoneNumber,
						staffTitle = user.Title,
                        staffName = user.UserName
                    });

                    return returnJson;
                }

                if (result.RequiresTwoFactor) {
                    return returnNoVaildJson("Requires Two Factor.");
                }

                if (result.IsLockedOut) {
                    return returnNoVaildJson("User account locked out.");
                } else {
                    return returnNoVaildJson("Invalid login attempt.");
                }
            } else {
                return returnNoVaildJson(ModelState.Values.First().Errors.First().ErrorMessage);
            }
        }


    }
}