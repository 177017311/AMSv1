﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace AMS.Controllers {
    public class APIController : Controller {
        public string returnNoVaildJson(string msg) {
            string returnJson = Newtonsoft.Json.JsonConvert.SerializeObject(new {
                status = ReturnStatus.fail,
                message = msg
            });

            return returnJson;
        }

        public string returnAccountNotFoundJson() {
            string returnJson = Newtonsoft.Json.JsonConvert.SerializeObject(new {
                status = ReturnStatus.fail,
                message = "Account Not Vaild"
            });

            return returnJson;
        }

        public string returnSuccessJson(string msg) {
            string returnJson = Newtonsoft.Json.JsonConvert.SerializeObject(new {
                status = ReturnStatus.success,
                message = msg
            });

            return returnJson;
        }

    }
}
