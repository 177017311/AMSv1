using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AMS.Models;
using AMS.Data;
using Microsoft.AspNetCore.Authorization;

namespace AMS.Controllers
{
    [Authorize]
    public class AbsenceRecordController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AbsenceRecordController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: AbsenceRecord
        public async Task<IActionResult> Index()
        {
            return View(await _context.AbsenceRecord.ToListAsync());
        }

        // GET: AbsenceRecord/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var absenceRecord = await _context.AbsenceRecord
                .SingleOrDefaultAsync(m => m.ID == id);
            if (absenceRecord == null)
            {
                return NotFound();
            }

            return View(absenceRecord);
        }

        // GET: AbsenceRecord/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: AbsenceRecord/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,LeaveType,LeaveStartDate,LeaveEndDate,Remark,Status")] AbsenceRecord absenceRecord)
        {
            if (ModelState.IsValid)
            {
                _context.Add(absenceRecord);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(absenceRecord);
        }

        // GET: AbsenceRecord/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var absenceRecord = await _context.AbsenceRecord.SingleOrDefaultAsync(m => m.ID == id);
            if (absenceRecord == null)
            {
                return NotFound();
            }
            return View(absenceRecord);
        }

        // POST: AbsenceRecord/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,LeaveType,LeaveStartDate,LeaveEndDate,Remark,Status")] AbsenceRecord absenceRecord)
        {
            if (id != absenceRecord.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(absenceRecord);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AbsenceRecordExists(absenceRecord.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(absenceRecord);
        }

        // GET: AbsenceRecord/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var absenceRecord = await _context.AbsenceRecord
                .SingleOrDefaultAsync(m => m.ID == id);
            if (absenceRecord == null)
            {
                return NotFound();
            }

            return View(absenceRecord);
        }

        // POST: AbsenceRecord/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var absenceRecord = await _context.AbsenceRecord.SingleOrDefaultAsync(m => m.ID == id);
            _context.AbsenceRecord.Remove(absenceRecord);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AbsenceRecordExists(int id)
        {
            return _context.AbsenceRecord.Any(e => e.ID == id);
        }
    }
}
