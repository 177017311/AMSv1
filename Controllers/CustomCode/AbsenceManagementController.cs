﻿using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Authentication;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using AMS.Models;
using AMS.Models.AccountViewModels;
using AMS.Services;
using AMS.Data;
using System.Collections.Generic;
using System;

namespace AMS.Controllers {

	public class AbsenceManagementController : Controller {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

		public AbsenceManagementController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            ILogger<AccountController> logger,
            ApplicationDbContext context) {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
            _context = context;
        }

        [TempData]
        public string ErrorMessage { get; set; }

        [Authorize]
		[HttpGet]
        [HttpPost]
		public async Task<IActionResult> Index(StaffAbsenceListViewModel _staffAbsenceListViewModel) {
			StaffAbsenceListViewModel staffAbsenceListViewModel = new StaffAbsenceListViewModel();

            var userId = _userManager.GetUserId(User);
            var ApplicationUser = await _userManager.FindByIdAsync(userId);

   //         Organization organization = _context.Users.Where(i => i.Id == ApplicationUser.Id).Include(o => o.Organization).Single().Organization;
            
			//staffAbsenceListViewModel.division = organization.Division;
			//staffAbsenceListViewModel.department = organization.Department;

            Organization organization = null;

            if (string.IsNullOrEmpty(_staffAbsenceListViewModel.selectedDept)) {
                organization = _context.Users.Where(i => i.Id == ApplicationUser.Id).Include(o => o.Organization).Single().Organization;

                staffAbsenceListViewModel.division = organization.Division;
                staffAbsenceListViewModel.department = organization.Department;
            } else {
                string[] org = _staffAbsenceListViewModel.selectedDept.Split('/');
                staffAbsenceListViewModel.division = org[0];
                staffAbsenceListViewModel.department = org[1];
                organization = _context.Organization.Where(o => o.Division == org[0] && o.Department == org[1]).Single();
            }

            List<Organization> organizations = _context.Organization.ToList();
            foreach (var dept in organizations.Where(o => !string.IsNullOrEmpty(o.Department)).ToList()) {
                staffAbsenceListViewModel.deptList.Add(dept.deptTitle());
            }
            staffAbsenceListViewModel.selectedDept = organization.deptTitle();

			staffAbsenceListViewModel.monthList = new Helper().getAllMonthBystring();
			if (string.IsNullOrEmpty(_staffAbsenceListViewModel.selectedMonth)) {
				_staffAbsenceListViewModel.selectedMonth = DateTime.Now.Month.ToString().PadLeft(2,'0');
            }

			staffAbsenceListViewModel.absenceStatusList = new Helper().getAllAbsenceStatus();
			if (string.IsNullOrEmpty(_staffAbsenceListViewModel.selectedAbsenceStatus)) {
				_staffAbsenceListViewModel.selectedAbsenceStatus = AbsenceStatus.created;
            }
            
            
			List<ApplicationUser> applicationUsers = _context.Users.Where(o => o.Organization == organization).Include(i => i.AbsenceRecords).ToList();
            foreach (ApplicationUser user in applicationUsers) {
				foreach (AbsenceRecord absenceRecord in user.AbsenceRecords.Where(o=>o.Status == _staffAbsenceListViewModel.selectedAbsenceStatus)) {
					StaffAbsenceListItemViewModel staffAbsenceListItemViewModel = new StaffAbsenceListItemViewModel();
					staffAbsenceListItemViewModel.id = absenceRecord.ID.ToString();
					staffAbsenceListItemViewModel.leaveStartDate = absenceRecord.LeaveStartDate.ToString(common.DateFormat);
					staffAbsenceListItemViewModel.leaveEndDate = absenceRecord.LeaveEndDate.ToString(common.DateFormat);
					staffAbsenceListItemViewModel.leaveType = absenceRecord.LeaveType;
					staffAbsenceListItemViewModel.remark = absenceRecord.Remark;
                    staffAbsenceListItemViewModel.staffName = user.StaffName;
					staffAbsenceListItemViewModel.staffTitle = user.Title;
					staffAbsenceListItemViewModel.status = absenceRecord.Status;

					staffAbsenceListViewModel.staffAbsenceList.Add(staffAbsenceListItemViewModel);
				}
            }

			return View(staffAbsenceListViewModel);
        }

		// GET: SystemParameter/Edit/5
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Update(string id) {
			if (string.IsNullOrEmpty(id)) {
				return NotFound();
			}
            
			UpdateStaffAbsenceRecordViewModel updateStaffAbsenceRecordViewModel = new UpdateStaffAbsenceRecordViewModel();

			AbsenceRecord absenceRecord = _context.AbsenceRecord.Include(j=>j.ApplicationUser).ThenInclude(m=>m.Organization).Where(o => o.ID == int.Parse(id)).SingleOrDefault();

            if (absenceRecord == null) {
                return NotFound();
            }
         
			updateStaffAbsenceRecordViewModel.id = absenceRecord.ID.ToString();
            updateStaffAbsenceRecordViewModel.staffName = absenceRecord.ApplicationUser.StaffName;
			updateStaffAbsenceRecordViewModel.staffTitle = absenceRecord.ApplicationUser.Title;         
			updateStaffAbsenceRecordViewModel.division = absenceRecord.ApplicationUser.Organization.Division;
			updateStaffAbsenceRecordViewModel.department = absenceRecord.ApplicationUser.Organization.Department;
            updateStaffAbsenceRecordViewModel.status = absenceRecord.Status;
			updateStaffAbsenceRecordViewModel.leaveType = absenceRecord.LeaveType;
			updateStaffAbsenceRecordViewModel.leaveStartDate = absenceRecord.LeaveStartDate.ToString(common.DateFormat);
			updateStaffAbsenceRecordViewModel.leaveEndDate = absenceRecord.LeaveEndDate.ToString(common.DateFormat);
			updateStaffAbsenceRecordViewModel.remark = absenceRecord.Remark;

            updateStaffAbsenceRecordViewModel.managerRemark = absenceRecord.ManagerRemark;

            updateStaffAbsenceRecordViewModel.image = Server.path + Server.file + absenceRecord.image;

			Helper helper = new Helper();
			updateStaffAbsenceRecordViewModel.actionList = helper.getAbsenceApprovalStatus();
			updateStaffAbsenceRecordViewModel.selectedAction = AbsenceStatus.approved;
   
			return View(updateStaffAbsenceRecordViewModel);
        }


        [Authorize]
		[HttpPost]
        [ValidateAntiForgeryToken]
		public async Task<IActionResult> Update(UpdateStaffAbsenceRecordViewModel updateStaffAbsenceRecordViewModel) {

			if (updateStaffAbsenceRecordViewModel == null) {
                return NotFound();
            }

			AbsenceRecord absenceRecord = _context.AbsenceRecord.Where(o => o.ID == int.Parse(updateStaffAbsenceRecordViewModel.id)).SingleOrDefault();
			absenceRecord.Status = updateStaffAbsenceRecordViewModel.selectedAction;
            absenceRecord.ManagerRemark = updateStaffAbsenceRecordViewModel.managerRemark;
			try {
				_context.Update(absenceRecord);
                await _context.SaveChangesAsync();
            } catch (DbUpdateConcurrencyException) {
				if (!AbsenceRecordExists(int.Parse(updateStaffAbsenceRecordViewModel.id))) {
                    return NotFound();
                } else {
                    throw;
                }
            }

            return RedirectToAction(nameof(Index));
        }

		private bool AbsenceRecordExists(int id) {
			return _context.AbsenceRecord.Any(e => e.ID == id);
        }

    }
}