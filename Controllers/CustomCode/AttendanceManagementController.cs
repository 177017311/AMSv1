﻿
using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Authentication;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using AMS.Models;
using AMS.Models.AccountViewModels;
using AMS.Services;
using AMS.Data;
using System.Collections.Generic;
using System;
using System.Globalization;

namespace AMS.Controllers {
   
    public class AttendanceManagementController : Controller {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public AttendanceManagementController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            ILogger<AccountController> logger,
            ApplicationDbContext context) {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
            _context = context;
        }

        [TempData]
        public string ErrorMessage { get; set; }

        [Authorize]
		[HttpGet]
		[HttpPost]
		public async Task<IActionResult> Index(StaffAttendanceListViewModel _staffListViewModel) {

			StaffAttendanceListViewModel staffListViewModel = new StaffAttendanceListViewModel();

            var userId = _userManager.GetUserId(User);
            var ApplicationUser = await _userManager.FindByIdAsync(userId);         

            //Organization organization = _context.Users.Where(i => i.Id == ApplicationUser.Id).Include(o => o.Organization).Single().Organization;

            //staffListViewModel.division = organization.Division;
            //staffListViewModel.department = organization.Department;

            Organization organization = null;

            if (string.IsNullOrEmpty(_staffListViewModel.selectedDept)) {
                organization = _context.Users.Where(i => i.Id == ApplicationUser.Id).Include(o => o.Organization).Single().Organization;

                staffListViewModel.division = organization.Division;
                staffListViewModel.department = organization.Department;
            } else {
                string[] org = _staffListViewModel.selectedDept.Split('/');
                staffListViewModel.division = org[0];
                staffListViewModel.department = org[1];
                organization = _context.Organization.Where(o => o.Division == org[0] && o.Department == org[1]).Single();
            }

            List<Organization> organizations = _context.Organization.ToList();
            foreach (var dept in organizations.Where(o => !string.IsNullOrEmpty(o.Department)).ToList()) {
                staffListViewModel.deptList.Add(dept.deptTitle());
            }
            staffListViewModel.selectedDept = organization.deptTitle();



			staffListViewModel.dateList = new Helper().getAllDatesBystring(DateTime.Now.Year, DateTime.Now.Month);
			if (string.IsNullOrEmpty(_staffListViewModel.selectedDate)){
				staffListViewModel.selectedDate = DateTime.Now.ToString(common.DateFormat);
			} else {
				staffListViewModel.selectedDate = _staffListViewModel.selectedDate;      
			}


			List<ApplicationUser> applicationUsers = _context.Users.Where(o => o.Organization == organization).Include(i => i.AttendanceRecords).Include(i => i.AbsenceRecords).ToList();
            foreach (ApplicationUser user in applicationUsers){
				StaffAttendanceListItemViewModel staffAttendanceListItemViewModel = new StaffAttendanceListItemViewModel();
				staffAttendanceListItemViewModel.id = user.Id;
                staffAttendanceListItemViewModel.StaffName = user.StaffName;
				staffAttendanceListItemViewModel.StaffTitle = user.Title;

				DailyAttendanceRecord dailyAttendanceRecord = user.GetDailyAttendance(DateTime.ParseExact(staffListViewModel.selectedDate,common.DateFormat, CultureInfo.InvariantCulture));
                staffAttendanceListItemViewModel.TodayLeaveTime = dailyAttendanceRecord.TodayLeaveTime == null ? string.Empty : dailyAttendanceRecord.TodayLeaveTime.Value.ToString(common.TimeFormat);
                staffAttendanceListItemViewModel.TodayWorkTime = dailyAttendanceRecord.TodayWorkTime == null ? string.Empty : dailyAttendanceRecord.TodayWorkTime.Value.ToString(common.TimeFormat);
				staffAttendanceListItemViewModel.TodayStatus = dailyAttendanceRecord.Status;
				staffAttendanceListItemViewModel.IsLate = dailyAttendanceRecord.IsLate;
				staffAttendanceListItemViewModel.AbsenceType = dailyAttendanceRecord.AbsenceType;

				staffListViewModel.staffAttendanceList.Add(staffAttendanceListItemViewModel);
            }

            return View(staffListViewModel);
        }

        // GET: SystemParameter/Edit/5
        [Authorize]
		[HttpGet]
        public async Task<IActionResult> Update(string id , string date) {
            if (string.IsNullOrEmpty(id)) {
                return NotFound();
            }

			if (string.IsNullOrEmpty(date)) {
                return NotFound();
            }

			UpdateStaffAttendanceRecordViewModel updateStaffAttendanceRecordViewModel = new UpdateStaffAttendanceRecordViewModel();

			var ApplicationUser = _context.Users.Where(o => o.Id == id).Include(i => i.Organization).Include(j=>j.AttendanceRecords).SingleOrDefault();

			updateStaffAttendanceRecordViewModel.id = ApplicationUser.Id;
            updateStaffAttendanceRecordViewModel.staffName = ApplicationUser.StaffName;
			updateStaffAttendanceRecordViewModel.staffTitle = ApplicationUser.Title;


			updateStaffAttendanceRecordViewModel.division = ApplicationUser.Organization.Division;
			updateStaffAttendanceRecordViewModel.department = ApplicationUser.Organization.Department;
			updateStaffAttendanceRecordViewModel.selectedDate = date;
			updateStaffAttendanceRecordViewModel.dutyHour = OfficeTime.dutyHour.ToString().PadLeft(2, '0');
			updateStaffAttendanceRecordViewModel.dutyMin = OfficeTime.dutyMin.ToString().PadLeft(2, '0');
			updateStaffAttendanceRecordViewModel.offHour = OfficeTime.offHour.ToString().PadLeft(2, '0');
			updateStaffAttendanceRecordViewModel.offMin = OfficeTime.offMin.ToString().PadLeft(2, '0');
			Helper helper = new Helper();
			updateStaffAttendanceRecordViewModel.hourList = helper.getHourList();
			updateStaffAttendanceRecordViewModel.minList = helper.getMinList();

			List<AttendanceRecord> attendanceRecords = ApplicationUser.AttendanceRecords.Where(o => o.RecordTimeStamp.Date == (DateTime.ParseExact(date, common.DateFormat, CultureInfo.InvariantCulture))).ToList();

			foreach (AttendanceRecord attendanceRecord in  attendanceRecords){
				AttendanceRecordViewModel attendanceRecordViewModel = new AttendanceRecordViewModel();
				attendanceRecordViewModel.ID = attendanceRecord.ID;
				attendanceRecordViewModel.Location = attendanceRecord.Location;
				attendanceRecordViewModel.RecordTimeStamp = attendanceRecord.RecordTimeStamp.ToString(common.DateTimeFormat);
				attendanceRecordViewModel.Status = attendanceRecord.Status;
				updateStaffAttendanceRecordViewModel.attendanceRecordViewModels.Add(attendanceRecordViewModel);
			}


			return View(updateStaffAttendanceRecordViewModel);
        }

        [Authorize]
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Update(UpdateStaffAttendanceRecordViewModel updateStaffAttendanceRecordViewModel) {

			if (updateStaffAttendanceRecordViewModel == null){
				return NotFound();
			}

			var ApplicationUser = await _userManager.FindByIdAsync(updateStaffAttendanceRecordViewModel.id);         
			Helper helper = new Helper();

			AttendanceRecord dutyAttendanceRecord = new AttendanceRecord();
			dutyAttendanceRecord.ApplicationUser = ApplicationUser;
			dutyAttendanceRecord.HotspotLocation = _context.HotspotLocation.Where(o=>o.iBeaconLocationMajor == "9999" && o.iBeaconLocationMinor == "9999").SingleOrDefault();
			dutyAttendanceRecord.Location = common.InsertRecordLocationByHead;
			dutyAttendanceRecord.RecordTimeStamp = helper.stringToDateTime(updateStaffAttendanceRecordViewModel.selectedDate,updateStaffAttendanceRecordViewModel.dutyHour,updateStaffAttendanceRecordViewModel.dutyMin);
			dutyAttendanceRecord.Status = AttendanceStatus.created;
			_context.Add(dutyAttendanceRecord);

			AttendanceRecord offAttendanceRecord = new AttendanceRecord();
			offAttendanceRecord.ApplicationUser = ApplicationUser;
			offAttendanceRecord.HotspotLocation = _context.HotspotLocation.Where(o => o.iBeaconLocationMajor == "9999" && o.iBeaconLocationMinor == "9999").SingleOrDefault();
			offAttendanceRecord.Location = common.InsertRecordLocationByHead;
			offAttendanceRecord.RecordTimeStamp = helper.stringToDateTime(updateStaffAttendanceRecordViewModel.selectedDate, updateStaffAttendanceRecordViewModel.offHour, updateStaffAttendanceRecordViewModel.offMin);
			offAttendanceRecord.Status = AttendanceStatus.created;
			_context.Add(offAttendanceRecord);

            await _context.SaveChangesAsync();

			return RedirectToAction(nameof(Index));
		}

    }
}