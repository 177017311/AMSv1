﻿using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Authentication;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using AMS.Models;
using AMS.Models.AccountViewModels;
using AMS.Services;
using AMS.Data;
using System.Collections.Generic;
using System;
using System.Globalization;
using System.Data;
using System.IO;

namespace AMS.Controllers {

    public class ReportController : Controller {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public ReportController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            ILogger<AccountController> logger,
            ApplicationDbContext context) {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
            _context = context;
        }

        [TempData]
        public string ErrorMessage { get; set; }

        [Authorize]
        [HttpGet]
        [HttpPost]
        public async Task<IActionResult> Index(string view) {
            ReportViewModel reportViewModel = new ReportViewModel();

            var userId = _userManager.GetUserId(User);
            var ApplicationUser = await _userManager.FindByIdAsync(userId);

            Organization organization = _context.Users.Where(i => i.Id == ApplicationUser.Id).Include(o => o.Organization).Single().Organization;

            reportViewModel.division = organization.Division;
            reportViewModel.department = organization.Department;

            List<Organization> organizations = _context.Organization.ToList();

            foreach (var dept in organizations.Where(o => !string.IsNullOrEmpty(o.Department)).ToList()) {
                reportViewModel.deptList.Add(dept.deptTitle());
            }
            reportViewModel.selectedDept = organization.deptTitle();

            reportViewModel.MonthList = new Helper().getAllMonthBystring();
            reportViewModel.selectedMonth = DateTime.Now.Month.ToString().PadLeft(2, '0');

            reportViewModel.YearList = new Helper().getYearListBystring();
            reportViewModel.selectedYear = DateTime.Now.Year.ToString();

            if (view == "Monthly"){
                reportViewModel.IsMonthly = true;
            } else if (view == "Yearly") {
                reportViewModel.IsYearly = true;
            } else if (view == "UrgentList"){
                reportViewModel.IsUrgentList = true;  
            } else {
                return NotFound();
            }



            return View(reportViewModel);
        }

        [Authorize]
		[HttpPost]
        public async Task<IActionResult> GetMonthly(string selectedMonth , string selectedYear) {
            int month;
            int year;
            if (string.IsNullOrEmpty(selectedMonth)) {
                return NotFound();
            }

            if (string.IsNullOrEmpty(selectedYear)) {
                return NotFound();
            }

            try {
                year = int.Parse(selectedYear);
            } catch {
                return NotFound();
            }

            try {
                month = int.Parse(selectedMonth);
            } catch {
                return NotFound();
            }

            var userId = _userManager.GetUserId(User);
            var ApplicationUser = await _userManager.FindByIdAsync(userId);   


            Organization organization = _context.Users.Where(i => i.Id == userId).Include(o => o.Organization).Single().Organization;


            DataTable dataTable = new DataTable("Monthly");
            dataTable.Columns.Add("Department");
            dataTable.Columns.Add("Staff Name");
            dataTable.Columns.Add("Staff Title");
            dataTable.Columns.Add("Clockin Date");
            dataTable.Columns.Add("Clockin Location");
            dataTable.Columns.Add("Clockin Time");
            dataTable.Columns.Add("Clockout Time");
            dataTable.Columns.Add("Working Hour");
            dataTable.Columns.Add("OT Min");
            dataTable.Columns.Add("Is Late");
            dataTable.Columns.Add("Late Min");
            dataTable.Columns.Add("Is Leave");
            dataTable.Columns.Add("Leave Type");
            dataTable.Columns.Add("Is Working Day");

            List<ApplicationUser> applicationUsers = new List<ApplicationUser>();
            if (ApplicationUser.StaffType != StaffType.HRStaff){
                applicationUsers = _context.Users.Where(o => o.Organization == organization).Include(k => k.Organization).Include(i => i.AttendanceRecords).Include(i => i.AbsenceRecords).OrderBy(q => q.Organization.Section).OrderBy(q => q.Organization.Department).OrderBy(q => q.StaffName).ToList();
            } else {
                applicationUsers = _context.Users.Include(k => k.Organization).Include(i => i.AttendanceRecords).Include(i => i.AbsenceRecords).OrderBy(q => q.Organization.Section).OrderBy(q => q.Organization.Department).OrderBy(q => q.StaffName).ToList();
            }

            foreach (ApplicationUser user in applicationUsers) {

                int dayInMonth= DateTime.DaysInMonth(year, month);
                DateTime dateOfMonth =  new DateTime(year, month, 1);
                for (int i = 0; i < dayInMonth; i++){
                    DailyAttendanceRecord dailyAttendanceRecord = user.GetDailyAttendance(dateOfMonth);

                    string clockinDate = dailyAttendanceRecord.TodayWorkTime != null ? (string)((DateTime)dailyAttendanceRecord.TodayWorkTime).ToString(common.DateFormat) : string.Empty;
                    string clockinTime = dailyAttendanceRecord.TodayWorkTime != null ? (string)((DateTime)dailyAttendanceRecord.TodayWorkTime).ToString(common.TimeFormat) : string.Empty;
                    string clockinLocation = dailyAttendanceRecord.TodayWorkTimeLocation;
                    string clockoutTime = dailyAttendanceRecord.TodayLeaveTime != null ? (string)((DateTime)dailyAttendanceRecord.TodayLeaveTime).ToString(common.TimeFormat) : string.Empty;

                    DataRow row =
                    dataTable.Rows.Add(
                        user.Organization.Division + "/" + user.Organization.Department,
                        user.StaffName,
                        user.Title,
                        dateOfMonth.ToString(common.DateFormat),
                        clockinLocation,
                        clockinTime,
                        clockoutTime,
						Math.Round(user.WorkingHour(dateOfMonth),2),
                        user.OTMin(dateOfMonth),
                        user.IsLate(dateOfMonth)? "TRUE" : "FALSE",
                        user.LateMin(dateOfMonth),
                        user.IsAbsence(dateOfMonth)? "TRUE" : "FALSE",
						user.GetAbsenceType(dateOfMonth),
                        dateOfMonth.DayOfWeek != DayOfWeek.Saturday && dateOfMonth.DayOfWeek != DayOfWeek.Sunday ? "TRUE" : "FALSE"
                    );

                    dateOfMonth = dateOfMonth.AddDays(1);
                }


            }

            ClosedXMLHelper _excelHelper = new ClosedXMLHelper();
            string fileDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
			string fileName = "AMS_Montly" + "_" + selectedYear + "_" +selectedMonth +"_" + fileDateTime;

            MemoryStream fileStream = _excelHelper.ExportToExcel(dataTable, fileName,"Monthly",string.Empty,string.Empty);

            string file = fileName + ".xlsx";
            string fileType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            fileStream.Position = 0;
            return File(fileStream, fileType, file);
        }

        [Authorize]
		[HttpPost]
        public async Task<IActionResult> GetYearly(string selectedYear) {

            int year;
            if (string.IsNullOrEmpty(selectedYear)) {
                return NotFound();
            }

            try {
                year = int.Parse(selectedYear);
            } catch {
                return NotFound();
            }


            var userId = _userManager.GetUserId(User);
            var ApplicationUser = await _userManager.FindByIdAsync(userId);


            DataTable dataTable = new DataTable("Monthly");
            dataTable.Columns.Add("Department");
            dataTable.Columns.Add("Staff Name");
            dataTable.Columns.Add("Staff Title");
            dataTable.Columns.Add("Clockin Date");
            dataTable.Columns.Add("Clockin Location");
            dataTable.Columns.Add("Clockin Time");
            dataTable.Columns.Add("Clockout Time");
            dataTable.Columns.Add("Working Hour");
            dataTable.Columns.Add("OT Min");
            dataTable.Columns.Add("Is Late");
            dataTable.Columns.Add("Late Min");
            dataTable.Columns.Add("Is Leave");
            dataTable.Columns.Add("Leave Type");
            dataTable.Columns.Add("Is Working Day");
            List<ApplicationUser> applicationUsers = _context.Users.Include(k => k.Organization).Include(i => i.AttendanceRecords).Include(i => i.AbsenceRecords).OrderBy(q => q.Organization.Section).OrderBy(q => q.Organization.Department).OrderBy(q => q.StaffName).ToList();
            foreach (ApplicationUser user in applicationUsers) {
                
                DateTime startOfYear = new DateTime(year, 1, 1);
                DateTime endOfYear = new DateTime(year+1, 1, 1);

                while (startOfYear.Date != endOfYear) {

                    DailyAttendanceRecord dailyAttendanceRecord = user.GetDailyAttendance(startOfYear);

                        string clockinDate = dailyAttendanceRecord.TodayWorkTime != null ? (string)((DateTime)dailyAttendanceRecord.TodayWorkTime).ToString(common.DateFormat) : string.Empty;
                        string clockinTime = dailyAttendanceRecord.TodayWorkTime != null ? (string)((DateTime)dailyAttendanceRecord.TodayWorkTime).ToString(common.TimeFormat) : string.Empty;
                        string clockinLocation = dailyAttendanceRecord.TodayWorkTimeLocation;
                        string clockoutTime = dailyAttendanceRecord.TodayLeaveTime != null ? (string)((DateTime)dailyAttendanceRecord.TodayLeaveTime).ToString(common.TimeFormat) : string.Empty;

                        DataRow row =
                        dataTable.Rows.Add(
							user.Organization.Division + "/" + user.Organization.Department,
                            user.StaffName,
                            user.Title,
							startOfYear.ToString(common.DateFormat),
                            clockinLocation,
                            clockinTime,
                            clockoutTime,
							Math.Round(user.WorkingHour(startOfYear), 2),
							user.OTMin(startOfYear),
                            user.IsLate(startOfYear)? "TRUE" : "FALSE",
							user.LateMin(startOfYear),
                            user.IsAbsence(startOfYear) ? "TRUE" : "FALSE",
							user.GetAbsenceType(startOfYear),
							startOfYear.DayOfWeek != DayOfWeek.Saturday && startOfYear.DayOfWeek != DayOfWeek.Sunday ? "TRUE" : "FALSE"
                        );

                    startOfYear = startOfYear.AddDays(1);
                    }

            }



            ClosedXMLHelper _excelHelper = new ClosedXMLHelper();
            string fileDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
            string fileName = "AMS_Yearly" + "_" + fileDateTime;

            MemoryStream fileStream = _excelHelper.ExportToExcel(dataTable, fileName, "Yearly", string.Empty, string.Empty);

            string file = fileName + ".xlsx";
            string fileType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            fileStream.Position = 0;
            return File(fileStream, fileType, file);
        }


        [Authorize]
		[HttpPost]
        public async Task<IActionResult> GetStaffListUrgentUse() {

            var userId = _userManager.GetUserId(User);
            var ApplicationUser = await _userManager.FindByIdAsync(userId);


            Organization organization = _context.Users.Where(i => i.Id == userId).Include(o => o.Organization).Single().Organization;


            DataTable dataTable = new DataTable("StaffList");
            dataTable.Columns.Add("Department");
            dataTable.Columns.Add("Staff Name");
            dataTable.Columns.Add("Title");
            dataTable.Columns.Add("Phone No.");
            dataTable.Columns.Add("Last Location");
            dataTable.Columns.Add("Last Time");
            dataTable.Columns.Add("Status");


            List<ApplicationUser> applicationUsers = _context.Users.Include(k => k.Organization).Include(i => i.AttendanceRecords).Include(i => i.AbsenceRecords).OrderBy(q=>q.Organization.Section).OrderBy(q => q.Organization.Department).OrderBy(q => q.StaffName).ToList();

            DateTime now = DateTime.Now;
            foreach (ApplicationUser user in applicationUsers) {
                DailyAttendanceRecord dailyAttendanceRecord = user.GetDailyAttendance(now);
                AttendanceRecord LastRecord = user.GetLastRecord(now);

                DataRow row =
                    dataTable.Rows.Add(
                        user.Organization.Division + "/" + user.Organization.Department,
                        user.StaffName,
                        user.Title,
                        user.PhoneNumber,
                        LastRecord != null ? LastRecord.Location : string.Empty,
                        LastRecord != null ? LastRecord.RecordTimeStamp.ToString(common.TimeFormat) : string.Empty,
                        dailyAttendanceRecord.Status
                    );
            }

            ClosedXMLHelper _excelHelper = new ClosedXMLHelper();
            string fileDateTime = DateTime.Now.ToString("yyyyMMddHHmmss");
            string fileName = "StaffList" + "_" + fileDateTime;

            MemoryStream fileStream = _excelHelper.ExportToExcel(dataTable, fileName, "StaffList", string.Empty, string.Empty);

            string file = fileName + ".xlsx";
            string fileType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            fileStream.Position = 0;
            return File(fileStream, fileType, file);
        }
    }
}
