﻿using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Authentication;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using AMS.Models;
using AMS.Models.AccountViewModels;
using AMS.Services;
using AMS.Data;
using System.Collections.Generic;
using System;
using System.Globalization;
using System.Web;

namespace AMS.Controllers {

	public class StaffAccountManagementController : Controller {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

		public StaffAccountManagementController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            ILogger<AccountController> logger,
            ApplicationDbContext context) {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
            _context = context;
        }

        [TempData]
        public string ErrorMessage { get; set; }

        [Authorize]
        [HttpGet]
        [HttpPost]
		public async Task<IActionResult> Index(StaffAccountListViewModel _staffAccountListViewModel) {
            
			StaffAccountListViewModel staffAccountListViewModel = new StaffAccountListViewModel();

            var userId = _userManager.GetUserId(User);
            var ApplicationUser = await _userManager.FindByIdAsync(userId);

            Organization organization = null;

            if (string.IsNullOrEmpty(_staffAccountListViewModel.selectedDept)){
                organization = _context.Users.Where(i => i.Id == ApplicationUser.Id).Include(o => o.Organization).Single().Organization;

                staffAccountListViewModel.division = organization.Division;
                staffAccountListViewModel.department = organization.Department;
            } else {
                string[] org = _staffAccountListViewModel.selectedDept.Split('/');
                staffAccountListViewModel.division = org[0];
                staffAccountListViewModel.department = org[1];
                organization = _context.Organization.Where(o => o.Division == org[0] && o.Department == org[1]).Single();
            }

            List<Organization> organizations = _context.Organization.ToList();
			foreach (var dept in organizations.Where(o => !string.IsNullOrEmpty(o.Department)).ToList()){
				staffAccountListViewModel.deptList.Add(dept.deptTitle());
			}
			staffAccountListViewModel.selectedDept = organization.deptTitle();

            staffAccountListViewModel.statusList = new Helper().getStaffAccountStatus();
            if (string.IsNullOrEmpty(_staffAccountListViewModel.selectedStatus)){
                staffAccountListViewModel.selectedStatus = staffAccountListViewModel.statusList.First();
            }else {
                staffAccountListViewModel.selectedStatus = _staffAccountListViewModel.selectedStatus;
            }
            List<ApplicationUser> applicationUsers = null;
            if (staffAccountListViewModel.selectedStatus == StaffAccountStatus.Effective){
                applicationUsers = _context.Users.Where(o => o.Organization == organization && (o.EffectiveStart >= DateTime.Now || o.EffecticeEnd >= DateTime.Now)).Include(i => i.AttendanceRecords).Include(i => i.AbsenceRecords).ToList();
            }else {
                applicationUsers = _context.Users.Where(o => o.Organization == organization && (o.EffectiveStart < DateTime.Now && o.EffecticeEnd < DateTime.Now)).Include(i => i.AttendanceRecords).Include(i => i.AbsenceRecords).ToList();            
            }

            foreach (ApplicationUser user in applicationUsers) {
				StaffAccountListItemViewModel staffAccountListItemViewModel = new StaffAccountListItemViewModel();
				staffAccountListItemViewModel.id = user.Id;
                staffAccountListItemViewModel.staffName = user.StaffName;
				staffAccountListItemViewModel.staffTitle = user.Title;
                staffAccountListItemViewModel.staffType = user.StaffType;
				staffAccountListViewModel.staffAttendanceList.Add(staffAccountListItemViewModel);
            }

			return View(staffAccountListViewModel);
        }

        [Authorize]
        [HttpGet]
        public IActionResult Create() {
            StaffAccountItemViewModel staffAccountItemViewModel = new StaffAccountItemViewModel();
            staffAccountItemViewModel.Password = new Helper().GenerateRandomPassword(12);

            List<Organization> organizations = _context.Organization.ToList();
            foreach (var dept in organizations.Where(o => !string.IsNullOrEmpty(o.Department)).ToList()) {
                staffAccountItemViewModel.deptList.Add(dept.deptTitle());
            }
            staffAccountItemViewModel.selectedDept = staffAccountItemViewModel.deptList.First();

            staffAccountItemViewModel.staffTypeList = new Helper().getStaffType();
            staffAccountItemViewModel.selectedStaffType = staffAccountItemViewModel.staffTypeList.First();

            staffAccountItemViewModel.EffectiveStart = DateTime.Now.ToString(common.DateFormat);
            staffAccountItemViewModel.EffecticeEnd = DateTime.Now.AddYears(1).ToString(common.DateFormat);

            return View(staffAccountItemViewModel);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(StaffAccountItemViewModel staffAccountItemViewModel) {
            if (ModelState.IsValid) {
                //List<Organization> organizations = _context.Organization.ToList();
                //foreach (var dept in organizations.Where(o => !string.IsNullOrEmpty(o.Department)).ToList()) {
                //    staffAccountItemViewModel.deptList.Add(dept.deptTitle());
                //}
                //staffAccountItemViewModel.selectedDept = staffAccountItemViewModel.deptList.First();

                //staffAccountItemViewModel.staffTypeList = new Helper().getStaffType();
                //staffAccountItemViewModel.selectedStaffType = staffAccountItemViewModel.staffTypeList.First();

                DateTime effectiveStart;
                DateTime effecticeEnd;

                try{
                    effectiveStart = DateTime.ParseExact(staffAccountItemViewModel.EffectiveStart, common.DateFormat, CultureInfo.InvariantCulture);
                }catch(Exception ex){
                    ModelState.AddModelError("EffectiveStart", "Not Vaild Effectice End Date, Please input with correct format.");
                    return View(staffAccountItemViewModel);
                }

                try {
                    effecticeEnd = DateTime.ParseExact(staffAccountItemViewModel.EffecticeEnd, common.DateFormat, CultureInfo.InvariantCulture);
                } catch (Exception ex) {
                    ModelState.AddModelError("EffecticeEnd", "Not Vaild Effectice End Date, Please input with correct format.");
                    return View(staffAccountItemViewModel);
                }

                if (effectiveStart >= effecticeEnd ){
                    ModelState.AddModelError("EffecticeEnd", "End Date should be later then Start Date.");
                    return View(staffAccountItemViewModel);
                }

                string[] org = staffAccountItemViewModel.selectedDept.Split('/');
                Organization organization = _context.Organization.Where(i => i.Division == org[0] && i.Department == org[1]).SingleOrDefault();
                if (organization == null){
                    return View(staffAccountItemViewModel);
                }

                var user = new ApplicationUser {
                    UserName = staffAccountItemViewModel.Email,
                    Email = staffAccountItemViewModel.Email,
                    StaffName = staffAccountItemViewModel.Name,
                    StaffType = staffAccountItemViewModel.selectedStaffType, 
                    PhoneNumber = staffAccountItemViewModel.phoneNumber,
                    Title = staffAccountItemViewModel.Title,
                    EffectiveStart = effectiveStart, 
                    EffecticeEnd = effecticeEnd, 
                    Organization = organization
                };
                var result = await _userManager.CreateAsync(user, staffAccountItemViewModel.Password);
                if (result.Succeeded) {
                    _logger.LogInformation("User created a new account with password.");

                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callbackUrl = Url.EmailConfirmationLink(user.Id, code, Request.Scheme);
                    await _emailSender.SendEmailConfirmationAsync(staffAccountItemViewModel.Email, callbackUrl);
                    return RedirectToAction(nameof(Index));
                }else{
                    ViewBag.ErrorMsg = result.Errors.First().Description;
                    return View(staffAccountItemViewModel);
                }
            }

            return View(staffAccountItemViewModel);
        }

        //// GET: SystemParameter/Edit/5
        /// 
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Update(string id) {
            if (string.IsNullOrEmpty(id)) {
                return NotFound();
            }

            var applicationUser = await _userManager.FindByIdAsync(id);
            if (applicationUser == null){
                return NotFound();
            }


            StaffAccountItemViewModel staffAccountItemViewModel = new StaffAccountItemViewModel();
            staffAccountItemViewModel.Id = id;

            List<Organization> organizations = _context.Organization.ToList();
            foreach (var dept in organizations.Where(o => !string.IsNullOrEmpty(o.Department)).ToList()) {
                staffAccountItemViewModel.deptList.Add(dept.deptTitle());
            }

            var currentOrg = _context.Users.Where(o => o.Id == applicationUser.Id).Include(i => i.Organization).FirstOrDefault().Organization.deptTitle();
            staffAccountItemViewModel.selectedDept = currentOrg;

            staffAccountItemViewModel.staffTypeList = new Helper().getStaffType();
            staffAccountItemViewModel.selectedStaffType = applicationUser.StaffType;

            staffAccountItemViewModel.EffectiveStart = applicationUser.EffectiveStart.ToString(common.DateFormat);
            staffAccountItemViewModel.EffecticeEnd = applicationUser.EffecticeEnd.ToString(common.DateFormat);
            staffAccountItemViewModel.Email = applicationUser.Email;
            staffAccountItemViewModel.Name = applicationUser.StaffName;
            staffAccountItemViewModel.Title = applicationUser.Title;
            staffAccountItemViewModel.phoneNumber = applicationUser.PhoneNumber;
              return View(staffAccountItemViewModel);

        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(StaffAccountItemViewModel staffAccountItemViewModel) {
            if (ModelState.IsValid) {
                if (string.IsNullOrEmpty(staffAccountItemViewModel.Id)) {
                    return NotFound();
                }

                var applicationUser = _context.Users.Where(o => o.Id == staffAccountItemViewModel.Id).FirstOrDefault();
                if (applicationUser == null) {
                    return NotFound();
                }


                //List<Organization> organizations = _context.Organization.ToList();
                //foreach (var dept in organizations.Where(o => !string.IsNullOrEmpty(o.Department)).ToList()) {
                //    staffAccountItemViewModel.deptList.Add(dept.deptTitle());
                //}
                //staffAccountItemViewModel.selectedDept = staffAccountItemViewModel.deptList.First();

                //staffAccountItemViewModel.staffTypeList = new Helper().getStaffType();
                //staffAccountItemViewModel.selectedStaffType = staffAccountItemViewModel.staffTypeList.First();

                DateTime effectiveStart;
                DateTime effecticeEnd;

                try {
                    effectiveStart = DateTime.ParseExact(staffAccountItemViewModel.EffectiveStart, common.DateFormat, CultureInfo.InvariantCulture);
                } catch (Exception ex) {
                    ModelState.AddModelError("EffectiveStart", "Not Vaild Effectice End Date, Please input with correct format.");
                    return View(staffAccountItemViewModel);
                }

                try {
                    effecticeEnd = DateTime.ParseExact(staffAccountItemViewModel.EffecticeEnd, common.DateFormat, CultureInfo.InvariantCulture);
                } catch (Exception ex) {
                    ModelState.AddModelError("EffecticeEnd", "Not Vaild Effectice End Date, Please input with correct format.");
                    return View(staffAccountItemViewModel);
                }

                if (effectiveStart >= effecticeEnd) {
                    ModelState.AddModelError("EffecticeEnd", "End Date should be later then Start Date.");
                    return View(staffAccountItemViewModel);
                }

                string[] org = staffAccountItemViewModel.selectedDept.Split('/');
                Organization organization = _context.Organization.Where(i => i.Division == org[0] && i.Department == org[1]).SingleOrDefault();
                if (organization == null) {
                    return View(staffAccountItemViewModel);
                }


                applicationUser.StaffName = staffAccountItemViewModel.Name;
                applicationUser.StaffType = staffAccountItemViewModel.selectedStaffType;
                applicationUser.PhoneNumber = staffAccountItemViewModel.phoneNumber;
                applicationUser.Title = staffAccountItemViewModel.Title;
                applicationUser.EffectiveStart = effectiveStart;
                applicationUser.EffecticeEnd = effecticeEnd;
                applicationUser.Organization = organization;
     
                try {
                    _context.Update(applicationUser);
                    await _context.SaveChangesAsync();
                } catch (DbUpdateConcurrencyException) {
                        throw;
                }
                return RedirectToAction(nameof(Index));
            }

            return View(staffAccountItemViewModel);
        }

    }
}