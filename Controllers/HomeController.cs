﻿using System.Threading.Tasks;
using System.Linq;
using Microsoft.AspNetCore.Authentication;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using AMS.Models;
using AMS.Models.AccountViewModels;
using AMS.Services;
using AMS.Data;
using System.Collections.Generic;
using System;
using System.Globalization;

namespace AMS.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly ApplicationDbContext _context;

        public HomeController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IEmailSender emailSender,
            ILogger<AccountController> logger,
            ApplicationDbContext context) {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _logger = logger;
            _context = context;
        }

		[Authorize]
        public async Task<IActionResult>  Index()
        {
            StaffAttendanceListViewModel staffListViewModel = new StaffAttendanceListViewModel();
            
            var userId = _userManager.GetUserId(User);
            var ApplicationUser = await _userManager.FindByIdAsync(userId);


			if (ApplicationUser.StaffType == StaffType.ITAdmin) {
                return View(staffListViewModel);
            
			}

			List<ApplicationUser> applicationUsers = new List<ApplicationUser>();
			if (ApplicationUser.StaffType == StaffType.HRStaff){
				 applicationUsers = _context.Users.Include(i => i.AttendanceRecords).Include(i => i.AbsenceRecords).ToList();
 
			}else{
				Organization organization = _context.Users.Where(i => i.Id == ApplicationUser.Id).Include(o => o.Organization).Single().Organization;
           
                applicationUsers = _context.Users.Where(o => o.Organization == organization).Include(i => i.AttendanceRecords).Include(i => i.AbsenceRecords).ToList();
 
			}

            foreach (ApplicationUser user in applicationUsers) {
                StaffAttendanceListItemViewModel staffAttendanceListItemViewModel = new StaffAttendanceListItemViewModel();
                staffAttendanceListItemViewModel.id = user.Id;
                staffAttendanceListItemViewModel.StaffName = user.StaffName;
                staffAttendanceListItemViewModel.StaffTitle = user.Title;

                DailyAttendanceRecord dailyAttendanceRecord = user.GetDailyAttendance(DateTime.Now);
                staffAttendanceListItemViewModel.TodayLeaveTime = dailyAttendanceRecord.TodayLeaveTime == null ? string.Empty : dailyAttendanceRecord.TodayLeaveTime.Value.ToString(common.TimeFormat);
                staffAttendanceListItemViewModel.TodayWorkTime = dailyAttendanceRecord.TodayWorkTime == null ? string.Empty : dailyAttendanceRecord.TodayWorkTime.Value.ToString(common.TimeFormat);
				staffAttendanceListItemViewModel.WarningStatus = user.GetWarningStatus(DateTime.Now);

				if (!string.IsNullOrEmpty(staffAttendanceListItemViewModel.WarningStatus)) {
					staffListViewModel.staffAttendanceList.Add(staffAttendanceListItemViewModel);
				}
            }

            return View(staffListViewModel);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

    }
}
