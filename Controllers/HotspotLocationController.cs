using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AMS.Models;
using AMS.Data;
using Microsoft.AspNetCore.Authorization;

namespace AMS.Controllers
{
    [Authorize]
    public class HotspotLocationController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HotspotLocationController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: HotspotLocation
        public async Task<IActionResult> Index()
        {
            return View(await _context.HotspotLocation.ToListAsync());
        }

        // GET: HotspotLocation/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hotspotLocation = await _context.HotspotLocation
                .SingleOrDefaultAsync(m => m.ID == id);
            if (hotspotLocation == null)
            {
                return NotFound();
            }

            return View(hotspotLocation);
        }

        // GET: HotspotLocation/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: HotspotLocation/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,LocationCode,LocationDescription,iBeaconLocationMajor,iBeaconLocationMinor")] HotspotLocation hotspotLocation)
        {
            if (ModelState.IsValid)
            {
                _context.Add(hotspotLocation);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(hotspotLocation);
        }

        // GET: HotspotLocation/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hotspotLocation = await _context.HotspotLocation.SingleOrDefaultAsync(m => m.ID == id);
            if (hotspotLocation == null)
            {
                return NotFound();
            }
            return View(hotspotLocation);
        }

        // POST: HotspotLocation/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,LocationCode,LocationDescription,iBeaconLocationMajor,iBeaconLocationMinor")] HotspotLocation hotspotLocation)
        {
            if (id != hotspotLocation.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(hotspotLocation);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HotspotLocationExists(hotspotLocation.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(hotspotLocation);
        }

        // GET: HotspotLocation/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hotspotLocation = await _context.HotspotLocation
                .SingleOrDefaultAsync(m => m.ID == id);
            if (hotspotLocation == null)
            {
                return NotFound();
            }

            return View(hotspotLocation);
        }

        // POST: HotspotLocation/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var hotspotLocation = await _context.HotspotLocation.SingleOrDefaultAsync(m => m.ID == id);
            _context.HotspotLocation.Remove(hotspotLocation);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HotspotLocationExists(int id)
        {
            return _context.HotspotLocation.Any(e => e.ID == id);
        }
    }
}
