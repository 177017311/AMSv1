using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AMS.Models;
using AMS.Data;
using Microsoft.AspNetCore.Authorization;

namespace AMS.Controllers
{
    [Authorize]
    public class SystemParameterController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SystemParameterController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: SystemParameter
        public async Task<IActionResult> Index()
        {
            return View(await _context.SystemParameter.ToListAsync());
        }

        // GET: SystemParameter/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var systemParameter = await _context.SystemParameter
                .SingleOrDefaultAsync(m => m.ID == id);
            if (systemParameter == null)
            {
                return NotFound();
            }

            return View(systemParameter);
        }

        // GET: SystemParameter/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: SystemParameter/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Code,Value")] SystemParameter systemParameter)
        {
            if (ModelState.IsValid)
            {
                _context.Add(systemParameter);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(systemParameter);
        }

        // GET: SystemParameter/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var systemParameter = await _context.SystemParameter.SingleOrDefaultAsync(m => m.ID == id);
            if (systemParameter == null)
            {
                return NotFound();
            }
            return View(systemParameter);
        }

        // POST: SystemParameter/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Code,Value")] SystemParameter systemParameter)
        {
            if (id != systemParameter.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(systemParameter);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SystemParameterExists(systemParameter.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(systemParameter);
        }

        // GET: SystemParameter/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var systemParameter = await _context.SystemParameter
                .SingleOrDefaultAsync(m => m.ID == id);
            if (systemParameter == null)
            {
                return NotFound();
            }

            return View(systemParameter);
        }

        // POST: SystemParameter/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var systemParameter = await _context.SystemParameter.SingleOrDefaultAsync(m => m.ID == id);
            _context.SystemParameter.Remove(systemParameter);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SystemParameterExists(int id)
        {
            return _context.SystemParameter.Any(e => e.ID == id);
        }
    }
}
