﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using AMS.Models;

namespace AMS.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
         
        public DbSet<AMS.Models.Organization> Organization { get; set; }
        public DbSet<AMS.Models.AttendanceRecord> AttendanceRecord { get; set; }
        public DbSet<AMS.Models.HotspotLocation> HotspotLocation { get; set; }
        public DbSet<AMS.Models.AbsenceRecord> AbsenceRecord { get; set; }
        public DbSet<AMS.Models.SystemParameter> SystemParameter { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Organization>().ToTable("Organization");
            builder.Entity<Organization>().HasMany(z => z.ApplicationUsers);

            builder.Entity<ApplicationUser>().HasOne(z => z.Organization);
            builder.Entity<ApplicationUser>().HasMany(z => z.AttendanceRecords);
            builder.Entity<ApplicationUser>().HasMany(z => z.AbsenceRecords);

            builder.Entity<AbsenceRecord>().ToTable("AbsenceRecord");
            builder.Entity<AbsenceRecord>().HasOne(z => z.ApplicationUser);

            builder.Entity<AttendanceRecord>().ToTable("AttendanceRecord");
            builder.Entity<AttendanceRecord>().HasOne(z => z.ApplicationUser);
            builder.Entity<AttendanceRecord>().HasOne(z => z.HotspotLocation);

            builder.Entity<HotspotLocation>().ToTable("HotspotLocation");
            builder.Entity<HotspotLocation>().HasMany(z => z.AttendanceRecords);

            builder.Entity<SystemParameter>().ToTable("SystemParameter");
        }
    }
}
