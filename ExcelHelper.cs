﻿//using DocumentFormat.OpenXml.Packaging;
//using DocumentFormat.OpenXml.Spreadsheet;
//using System;
//using System.Collections.Generic;
//using System.Data;
//using System.IO;
//using System.Linq;

//namespace AMS {
//    public class ExcelHelper {


//        public DataTable ConvertToDataTableByOpenXML(MemoryStream fs) {
//            DataTable dt = new DataTable("DataTable");
//            using (SpreadsheetDocument doc = SpreadsheetDocument.Open(fs, false)) {

//                WorkbookPart workbookPart = doc.WorkbookPart;
//                SharedStringTablePart sstpart = workbookPart.GetPartsOfType<SharedStringTablePart>().First();
//                SharedStringTable sst = sstpart.SharedStringTable;

//                WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();
//                Worksheet sheet = worksheetPart.Worksheet;

//                var rows = sheet.Descendants<Row>();

//                foreach (Row row in rows) {
//                    IEnumerable<Cell> cells = SpreedsheetHelper.GetRowCells(row);
//                    if (row == rows.First()) {
//                        foreach (Cell cell in cells) {
//                            if ((cell.DataType != null) && (cell.DataType == CellValues.SharedString)) {
//                                int ssid = int.Parse(cell.CellValue.Text);
//                                string str = sst.ChildElements[ssid].InnerText;
//                                //Console.WriteLine("Shared string {0}: {1}", ssid, str);
//                                DataColumn newCol = new DataColumn(str, typeof(String));
//                                dt.Columns.Add(newCol);
//                            } else if (cell.CellValue != null) {
//                                //Console.WriteLine("Cell contents: {0}", c.CellValue.Text);
//                                string str = cell.CellValue.Text;
//                                DataColumn newCol = new DataColumn(str, typeof(String));
//                                dt.Columns.Add(newCol);
//                            }
//                        }
//                    } else {
//                        DataRow dtRow = dt.NewRow();

//                        int i = 0;

//                        foreach (Cell cell in cells) {
//                            if ((cell.DataType != null) && (cell.DataType == CellValues.SharedString)) {
//                                int ssid = int.Parse(cell.CellValue.Text);
//                                string str = sst.ChildElements[ssid].InnerText;
//                                //Console.WriteLine("Shared string {0}: {1}", ssid, str);
//                                dtRow[i] = str;
//                                i++;
//                            } else if ((cell.DataType != null) && (cell.DataType == CellValues.Boolean)) {
//                                string str = cell.CellValue.Text;
//                                if (str.Equals("0")) {
//                                    dtRow[i] = "FALSE";
//                                }
//                                if (str.Equals("1")) {
//                                    dtRow[i] = "TRUE";
//                                }
//                                i++;
//                            } else if (cell.CellValue != null) {
//                                //Console.WriteLine("Cell contents: {0}", c.CellValue.Text);
//                                string str = cell.CellValue.Text;
//                                dtRow[i] = str;
//                                i++;
//                            } else if (cell.CellValue == null) {
//                                dtRow[i] = null;
//                                i++;
//                            }
//                        }

//                        dt.Rows.Add(dtRow);
//                    }
//                }
//            }
//            return dt;
//        }

//        public string GetSubjectProperty(byte[] fileContent) {
//            using (MemoryStream fs = new MemoryStream(fileContent)) {
//                return GetSubjectPropertyByOpenXML(fs);
//            }
//        }

//        public string GetCategoriesProperty(byte[] fileContent) {
//            using (MemoryStream fs = new MemoryStream(fileContent)) {
//                return GetCategoriesPropertyByOpenXML(fs);
//            }
//        }

//        public string GetSubjectProperty(MemoryStream fileContent) {
//            return GetSubjectPropertyByOpenXML(fileContent);
//        }

//        public string GetCategoriesProperty(MemoryStream fileContent) {
//            return GetCategoriesPropertyByOpenXML(fileContent);
//        }


//        public string GetSubjectPropertyByOpenXML(MemoryStream fs) {
//            using (SpreadsheetDocument doc = SpreadsheetDocument.Open(fs, false)) {
//                return doc.PackageProperties.Subject;
//            }
//        }

//        public string GetCategoriesPropertyByOpenXML(MemoryStream fs) {
//            using (SpreadsheetDocument doc = SpreadsheetDocument.Open(fs, false)) {
//                return doc.PackageProperties.Category;
//            }
//        }

//        public DataTable ConvertToDataTable(byte[] fileContent) {
//            DataTable dt = new DataTable("DataTable");

//            using (MemoryStream fs = new MemoryStream(fileContent)) {
//                using (SpreadsheetDocument doc = SpreadsheetDocument.Open(fs, false)) {

//                    WorkbookPart workbookPart = doc.WorkbookPart;
//                    SharedStringTablePart sstpart = workbookPart.GetPartsOfType<SharedStringTablePart>().First();
//                    SharedStringTable sst = sstpart.SharedStringTable;

//                    WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();
//                    Worksheet sheet = worksheetPart.Worksheet;

//                    var rows = sheet.Descendants<Row>();

//                    foreach (Row row in rows) {
//                        IEnumerable<Cell> cells = SpreedsheetHelper.GetRowCells(row);
//                        if (row == rows.First()) {
//                            foreach (Cell cell in cells) {
//                                if ((cell.DataType != null) && (cell.DataType == CellValues.SharedString)) {
//                                    int ssid = int.Parse(cell.CellValue.Text);
//                                    string str = sst.ChildElements[ssid].InnerText;
//                                    //Console.WriteLine("Shared string {0}: {1}", ssid, str);
//                                    DataColumn newCol = new DataColumn(str, typeof(String));
//                                    dt.Columns.Add(newCol);
//                                } else if (cell.CellValue != null) {
//                                    //Console.WriteLine("Cell contents: {0}", c.CellValue.Text);
//                                    string str = cell.CellValue.Text;
//                                    DataColumn newCol = new DataColumn(str, typeof(String));
//                                    dt.Columns.Add(newCol);
//                                }
//                            }
//                        } else {
//                            DataRow dtRow = dt.NewRow();

//                            int i = 0;

//                            foreach (Cell cell in cells) {
//                                if ((cell.DataType != null) && (cell.DataType == CellValues.SharedString)) {
//                                    int ssid = int.Parse(cell.CellValue.Text);
//                                    string str = sst.ChildElements[ssid].InnerText;
//                                    //Console.WriteLine("Shared string {0}: {1}", ssid, str);
//                                    dtRow[i] = str;
//                                    i++;
//                                } else if ((cell.DataType != null) && (cell.DataType == CellValues.Boolean)) {
//                                    string str = cell.CellValue.Text;
//                                    if (str.Equals("0")) {
//                                        dtRow[i] = "FALSE";
//                                    }
//                                    if (str.Equals("1")) {
//                                        dtRow[i] = "TRUE";
//                                    }
//                                    i++;
//                                } else if (cell.CellValue != null) {
//                                    //Console.WriteLine("Cell contents: {0}", c.CellValue.Text);
//                                    string str = cell.CellValue.Text;
//                                    dtRow[i] = str;
//                                    i++;
//                                } else if (cell.CellValue == null) {
//                                    dtRow[i] = null;
//                                    i++;
//                                }
//                            }

//                            dt.Rows.Add(dtRow);
//                        }
//                    }
//                }
//            }

//            return dt;
//        }


//        public DataTable ConvertToDataTable(MemoryStream fileContent) {
//            DataTable dt = new DataTable("DataTable");

//            dt = ConvertToDataTableByOpenXML(fileContent);

//            return dt;
//        }

//        public DataTable ConvertToDataTable(byte[] fileContent, string targetSheetName) {
//            using (MemoryStream fs = new MemoryStream(fileContent)) {
//                return ConvertToDataTable(fs, targetSheetName);
//            }
//        }
//        private WorksheetPart GetWorksheetFromSheetName(WorkbookPart workbookPart, string sheetName) {
//            Sheet sheet = workbookPart.Workbook.Descendants<Sheet>().FirstOrDefault(s => s.Name == sheetName);
//            if (sheet == null) throw new Exception(string.Format("Could not find sheet with name {0}", sheetName));
//            else return workbookPart.GetPartById(sheet.Id) as WorksheetPart;
//        }
//        public DataTable ConvertToDataTable(MemoryStream stream, string targetSheetName) {
//            DataTable dt = new DataTable("DataTable");

//            using (SpreadsheetDocument doc = SpreadsheetDocument.Open(stream, false)) {
//                WorkbookPart workbookPart = doc.WorkbookPart;
//                SharedStringTablePart sstpart = workbookPart.GetPartsOfType<SharedStringTablePart>().First();
//                SharedStringTable sst = sstpart.SharedStringTable;

//                WorksheetPart worksheetPart = GetWorksheetFromSheetName(workbookPart, targetSheetName);

//                if (worksheetPart == null) {
//                    return null;
//                } else {
//                    Worksheet sheet = worksheetPart.Worksheet;

//                    var rows = sheet.Descendants<Row>();

//                    foreach (Row row in rows) {
//                        IEnumerable<Cell> cells = SpreedsheetHelper.GetRowCells(row);
//                        if (row == rows.First()) {
//                            foreach (Cell cell in cells) {
//                                if ((cell.DataType != null) && (cell.DataType == CellValues.SharedString)) {
//                                    int ssid = int.Parse(cell.CellValue.Text);
//                                    string str = sst.ChildElements[ssid].InnerText;
//                                    //Console.WriteLine("Shared string {0}: {1}", ssid, str);
//                                    DataColumn newCol = new DataColumn(str, typeof(String));
//                                    dt.Columns.Add(newCol);
//                                } else if (cell.CellValue != null) {
//                                    //Console.WriteLine("Cell contents: {0}", c.CellValue.Text);
//                                    string str = cell.CellValue.Text;
//                                    DataColumn newCol = new DataColumn(str, typeof(String));
//                                    dt.Columns.Add(newCol);
//                                }
//                            }
//                        } else {
//                            DataRow dtRow = dt.NewRow();

//                            int i = 0;

//                            foreach (Cell cell in cells) {
//                                if ((cell.DataType != null) && (cell.DataType == CellValues.SharedString)) {
//                                    int ssid = int.Parse(cell.CellValue.Text);
//                                    string str = sst.ChildElements[ssid].InnerText;
//                                    //Console.WriteLine("Shared string {0}: {1}", ssid, str);
//                                    dtRow[i] = str;
//                                    i++;
//                                } else if ((cell.DataType != null) && (cell.DataType == CellValues.Boolean)) {
//                                    string str = cell.CellValue.Text;
//                                    if (str.Equals("0")) {
//                                        dtRow[i] = "FALSE";
//                                    }
//                                    if (str.Equals("1")) {
//                                        dtRow[i] = "TRUE";
//                                    }
//                                    i++;
//                                } else if (cell.CellValue != null) {
//                                    //Console.WriteLine("Cell contents: {0}", c.CellValue.Text);
//                                    string str = cell.CellValue.Text;
//                                    dtRow[i] = str;
//                                    i++;
//                                } else if (cell.CellValue == null) {
//                                    dtRow[i] = null;
//                                    i++;
//                                }
//                            }

//                            dt.Rows.Add(dtRow);
//                        }
//                    }
//                    return dt;
//                }
//            }
//        }

 

//        public bool ContainColumn(string columnName, DataTable table) {
//            DataColumnCollection columns = table.Columns;
//            if (columns.Contains(columnName)) {
//                return true;
//            } else {
//                return false;
//            }
//        }

//        public bool CheckBooleanValue(string boolValue) {
//            if (boolValue.Equals("Y", StringComparison.OrdinalIgnoreCase) ||
//                boolValue.Equals("YES", StringComparison.OrdinalIgnoreCase) ||
//                boolValue.Equals("TRUE", StringComparison.OrdinalIgnoreCase)) {
//                return true;
//            }

//            if (boolValue.Equals("N", StringComparison.OrdinalIgnoreCase) ||
//                boolValue.Equals("NO", StringComparison.OrdinalIgnoreCase) ||
//                boolValue.Equals("FALSE", StringComparison.OrdinalIgnoreCase)) {
//                return false;
//            }

//            return false;
//        }
//    }


//}
