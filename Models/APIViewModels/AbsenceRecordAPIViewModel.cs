﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.Models {
    public class AbsenceRecordAPIViewModel {

        [Required]
        public string Id { get; set; }
        
        public string absenceID { get; set; }

        [Required]
        public string leaveType { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime leaveStartDate { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime leaveEndDate { get; set; }

        [Required]
        public string remark { get; set; }

        public string managerRemark { get; set; }

        public string approveStatus { get; set; }

        public string image { get; set; }
        public string imagePath { get; set; }



    }
}