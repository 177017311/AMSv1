﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.Models {
    public class AttendanceAPIViewModel {

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime attendanceTimeStamp { get; set; }

        [Required]
        public string attendanceID { get; set; }

        [Required]
        public string Id { get; set; }

        [Required]
        public string iBeaconLocationMajor { get; set; }

        [Required]
        public string iBeaconLocationMinor { get; set; }


    }
}