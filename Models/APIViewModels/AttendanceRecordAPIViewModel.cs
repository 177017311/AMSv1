﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.Models {
    public class AttendanceRecordAPIViewModel {

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime attendanceTimeStamp { get; set; }

        [Required]
        public string location { get; set; }

        [Required]
        public string attendanceStatus { get; set; }


    }
}
