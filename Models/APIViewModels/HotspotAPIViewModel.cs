﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.Models {
	public class HotspotAPIViewModel {
  
		public int ID { get; set; }
        public string LocationCode { get; set; }
        public string LocationDescription { get; set; }
        public string iBeaconLocationMajor { get; set; }
        public string iBeaconLocationMinor { get; set; }

    }
}
