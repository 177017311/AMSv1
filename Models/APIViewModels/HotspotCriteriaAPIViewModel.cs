﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.Models {
	public class HotspotCriteriaAPIViewModel {

        [DataType(DataType.Date)]
		public DateTime? lastRecordDateTime { get; set; }
    }
}
