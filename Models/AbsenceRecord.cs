﻿using System;
namespace AMS.Models
{
    public class AbsenceRecord
    {
        public int ID { get; set; }
        public string LeaveType { get; set; }
        public DateTime LeaveStartDate { get; set; }
        public DateTime LeaveEndDate { get; set; }
        public string Remark { get; set; }
        public string Status { get; set; }
        public string ManagerRemark { get; set; }
        public string image { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
    }
}