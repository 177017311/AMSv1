﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.Models {
    public class StaffAccountViewModel {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        public string StaffType { get; set; }


        [Required]
        public string Name { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public DateTime EffectiveStart { get; set; }

        [Required]
        public DateTime EffecticeEnd { get; set; }

        [Required]
        public string Division { get; set; }

        [Required]
        public string Department { get; set; }

        [Required]
        public string Section { get; set; }

        public Organization organization { get; set; }

    }
}
