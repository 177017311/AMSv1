﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace AMS.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public partial class ApplicationUser : IdentityUser
    {
        public string StaffType { get; set; }
        public string Title { get; set; }
        public string StaffName { get; set; }
        public DateTime EffectiveStart { get; set; }
        public DateTime EffecticeEnd { get; set; }
        public Organization Organization { get; set; }
        public ICollection<AttendanceRecord> AttendanceRecords { get; set; }
        public ICollection<AbsenceRecord> AbsenceRecords { get; set; }
        public HotspotLocation HotspotLocation { get; set; }
    }
}
