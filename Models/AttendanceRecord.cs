﻿using System;
namespace AMS.Models
{
    public class AttendanceRecord
    {
        public int ID { get; set; }
        public string Location { get; set; }
        public DateTime RecordTimeStamp { get; set; }
        public string Status { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        public HotspotLocation HotspotLocation { get; set; }
    }
}