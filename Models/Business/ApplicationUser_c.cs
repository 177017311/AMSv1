﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace AMS.Models {
    // Add profile data for application users by adding properties to the ApplicationUser class
    public partial class ApplicationUser {

		int OfficeStartHour = OfficeTime.dutyHour;
		int OfficeStartMin = OfficeTime.dutyMin;
		int OfficeEndHour = OfficeTime.offHour;
		int OfficeEndMin = OfficeTime.offMin;
        int bufferMin = OfficeTime.bufferMin;
		int lunchHour = OfficeTime.lunchHour;

        TimeSpan bufferMinTS = new TimeSpan();
        TimeSpan startOfOfficeHour = new TimeSpan();
        TimeSpan endOfOfficeHour = new TimeSpan();
        TimeSpan startOfOfficeHourWithBufferTime = new TimeSpan();
        TimeSpan endOfOfficeHourWithBufferTime = new TimeSpan();

        public DailyAttendanceRecord GetDailyAttendance(DateTime? date){
            DailyAttendanceRecord dailyAttendanceRecord = new DailyAttendanceRecord();


            if (date == null){
                date = DateTime.Today;
            }

             bufferMinTS = new TimeSpan(0, OfficeTime.bufferMin, 0);
             startOfOfficeHour = new TimeSpan(OfficeStartHour, OfficeStartMin, 0);
             endOfOfficeHour = new TimeSpan(OfficeEndHour, OfficeEndMin, 0);
             startOfOfficeHourWithBufferTime = startOfOfficeHour.Add(bufferMinTS);
             endOfOfficeHourWithBufferTime = endOfOfficeHour.Add(bufferMinTS);

			AttendanceRecord TodayWorkTimeRecord = this.AttendanceRecords.Where(o => o.RecordTimeStamp.Date == date.Value.Date).ToList().FirstOrDefault();
			AttendanceRecord TodayLeaveTimeRecord = this.AttendanceRecords.Where(o => o.RecordTimeStamp.Date == date.Value.Date).ToList().LastOrDefault();

            dailyAttendanceRecord.TodayWorkTimeLocation = string.Empty;
            dailyAttendanceRecord.TodayLeaveTimeLocation = string.Empty;

            if (TodayWorkTimeRecord != null){
                dailyAttendanceRecord.TodayWorkTime = TodayWorkTimeRecord.RecordTimeStamp;
                dailyAttendanceRecord.TodayWorkTimeLocation = TodayWorkTimeRecord.Location;
            }

            if (TodayLeaveTimeRecord != null && TodayWorkTimeRecord != TodayLeaveTimeRecord) {
                if (TodayLeaveTimeRecord.RecordTimeStamp.TimeOfDay >= endOfOfficeHour) {
                    dailyAttendanceRecord.TodayLeaveTime = TodayLeaveTimeRecord.RecordTimeStamp;
                    dailyAttendanceRecord.TodayLeaveTimeLocation = TodayLeaveTimeRecord.Location;
                }else {
                    if (DateTime.Now.TimeOfDay >= endOfOfficeHour){
                        dailyAttendanceRecord.TodayLeaveTime = TodayLeaveTimeRecord.RecordTimeStamp;
                        dailyAttendanceRecord.TodayLeaveTimeLocation = TodayLeaveTimeRecord.Location;
                    }
                }
            }

			dailyAttendanceRecord.Status = this.CalcuateStatus(date);
			dailyAttendanceRecord.IsLate = this.IsLate(date);
			dailyAttendanceRecord.AbsenceType = this.GetAbsenceType(date);
            return dailyAttendanceRecord;
        }


        public string CalcuateStatus(DateTime? date) {
            if (date == null) {
                date = DateTime.Today;
            }


			AttendanceRecord TodayWorkTimeRecord = this.AttendanceRecords.Where(o => o.RecordTimeStamp.Date == date.Value.Date).ToList().FirstOrDefault();
			AttendanceRecord TodayLeaveTimeRecord = this.AttendanceRecords.Where(o => o.RecordTimeStamp.Date == date.Value.Date).ToList().LastOrDefault();


            if (TodayWorkTimeRecord == null){
                return AttendanceStatus.NoRecord;
            }


            return AttendanceStatus.OnDuty;

        }

        public bool IsAbsence(DateTime? date) {
            AbsenceRecord absenceRecord = this.AbsenceRecords.Where(o => date.Value.Date >= o.LeaveStartDate.Date && date.Value.Date <= o.LeaveEndDate.Date && o.Status == AbsenceStatus.approved).FirstOrDefault();
            if (absenceRecord != null) {
                return true;
            }
            return false;
        }

        public string GetAbsenceType(DateTime? date) {
            AbsenceRecord absenceRecord = this.AbsenceRecords.Where(o => date.Value.Date >= o.LeaveStartDate.Date && date.Value.Date <= o.LeaveEndDate.Date && o.Status == AbsenceStatus.approved).FirstOrDefault();
            if (absenceRecord != null) {
                return absenceRecord.LeaveType;
            }
            return string.Empty;
        }

        public bool IsLate(DateTime? date) {
            if (date == null) {
                date = DateTime.Today;
            }

			AttendanceRecord TodayWorkTimeRecord = this.AttendanceRecords.Where(o => o.RecordTimeStamp.Date == date.Value.Date).ToList().FirstOrDefault();

            if (TodayWorkTimeRecord == null) {
                return false;
            }

            DateTime selectedDate = (DateTime)date;
            //DateTime startOfOfficeHour = new DateTime(selectedDate.Year, selectedDate.Month, 1, OfficeStartHour, OfficeStartMin, 0);
            //DateTime startOfOfficeHourWithBufferTime = startOfOfficeHour.AddMinutes(bufferMin);


			TimeSpan startOfOfficeHour = new TimeSpan(OfficeStartHour, OfficeStartMin,0);
			TimeSpan bufferMinTS = new TimeSpan(0, bufferMin , 0); 
			TimeSpan startOfOfficeHourWithBufferTime = startOfOfficeHour.Add(bufferMinTS); 

			if (TodayWorkTimeRecord.RecordTimeStamp.TimeOfDay >= startOfOfficeHourWithBufferTime) {
                return true;
            }

            return false;
        }


        public double LateMin(DateTime? date) {
            if (date == null) {
                date = DateTime.Today;
            }

            DateTime selectedDate = (DateTime)date;

            if (this.IsLate(date)){
                DateTime startOfWorkingHour = new DateTime(selectedDate.Year, selectedDate.Month, 1,OfficeStartHour,OfficeStartMin,0);
				AttendanceRecord TodayWorkTimeRecord = this.AttendanceRecords.Where(o => o.RecordTimeStamp.Date == date.Value.Date).ToList().FirstOrDefault();
                return new Helper().GetCompareMin(startOfWorkingHour, TodayWorkTimeRecord.RecordTimeStamp);
            }

            return 0;
        }

        public double OTMin(DateTime? date) {
            if (date == null) {
                date = DateTime.Today;
            }
            
            DateTime selectedDate = (DateTime)date;
			DateTime startOfOfficeHour = new DateTime(selectedDate.Year, selectedDate.Month, selectedDate.Day, OfficeStartHour, OfficeStartMin, 0);
			DateTime endOfOfficeHour = new DateTime(selectedDate.Year, selectedDate.Month, selectedDate.Day, OfficeEndHour, OfficeEndMin, 0);
            DateTime endOfOfficeHourWithBufferTime = endOfOfficeHour.AddMinutes(bufferMin);

			AttendanceRecord TodayWorkingTimeRecord = this.AttendanceRecords.Where(o => o.RecordTimeStamp.Date == date.Value.Date).ToList().FirstOrDefault();
			AttendanceRecord TodayLeaveTimeRecord = this.AttendanceRecords.Where(o => o.RecordTimeStamp.Date == date.Value.Date).ToList().LastOrDefault();
         
            if (TodayLeaveTimeRecord == null) {
                return 0;
            }

            if (TodayLeaveTimeRecord.RecordTimeStamp >= endOfOfficeHourWithBufferTime) {

                if (this.IsLate(date)) {
					DateTime startOfOfficeHourdt = new DateTime(selectedDate.Year, selectedDate.Month, 1, OfficeStartHour, OfficeStartMin, 0);
                    DateTime endOfOfficeHourdt = new DateTime(selectedDate.Year, selectedDate.Month, 1, OfficeEndHour, OfficeEndMin, 0);
                    double dailyTotalOfficeHour = new Helper().GetCompareHour(startOfOfficeHourdt, endOfOfficeHourdt);

					double dailyWorkingHour = new Helper().GetCompareHour(TodayWorkingTimeRecord.RecordTimeStamp, TodayLeaveTimeRecord.RecordTimeStamp);
					if (dailyWorkingHour > dailyTotalOfficeHour){
						return (dailyTotalOfficeHour - dailyWorkingHour) * 60;
					} else {
						return 0;
					}

                } 
                

				return new Helper().GetCompareMin(TodayLeaveTimeRecord.RecordTimeStamp,endOfOfficeHour);
            }


            return 0;
        }

        public double WorkingHour(DateTime? date) {
            if (date == null) {
                date = DateTime.Today;
            }

            DateTime selectedDate = (DateTime)date;

            DateTime startOfOfficeHourdt = new DateTime(selectedDate.Year, selectedDate.Month, 1, OfficeStartHour, OfficeStartMin, 0);         
            DateTime endOfOfficeHourdt = new DateTime(selectedDate.Year, selectedDate.Month, 1, OfficeEndHour, OfficeEndMin, 0);

			bufferMinTS = new TimeSpan(0, OfficeTime.bufferMin, 0);
			startOfOfficeHour = new TimeSpan(OfficeStartHour, OfficeStartMin, 0);
			endOfOfficeHour = new TimeSpan(OfficeEndHour, OfficeEndMin, 0);
            startOfOfficeHourWithBufferTime = startOfOfficeHour.Add(bufferMinTS);
            endOfOfficeHourWithBufferTime = endOfOfficeHour.Add(bufferMinTS);

			double dailyTotalOfficeHour = new Helper().GetCompareHour(startOfOfficeHourdt, endOfOfficeHourdt);

			AttendanceRecord TodayWorkingTimeRecord = this.AttendanceRecords.Where(o => o.RecordTimeStamp.Date == date.Value.Date).ToList().FirstOrDefault();
			AttendanceRecord TodayLeaveTimeRecord = this.AttendanceRecords.Where(o => o.RecordTimeStamp.Date == date.Value.Date).ToList().LastOrDefault();

            if (TodayWorkingTimeRecord == null) {
                return 0;
            }

			AbsenceRecord absenceRecord = this.AbsenceRecords.Where(o => date.Value.Date >= o.LeaveStartDate && date.Value.Date <= o.LeaveEndDate && o.Status == AbsenceStatus.approved).FirstOrDefault();
            if (absenceRecord != null) {
				return dailyTotalOfficeHour - lunchHour;
            }

			if (TodayWorkingTimeRecord.RecordTimeStamp.TimeOfDay <= startOfOfficeHourWithBufferTime 
			    && TodayLeaveTimeRecord.RecordTimeStamp.TimeOfDay >= endOfOfficeHour){
                return dailyTotalOfficeHour;
            }

            if (this.IsLate(date)){
                double totalOfficeHour = new Helper().GetCompareHour(TodayWorkingTimeRecord.RecordTimeStamp, TodayLeaveTimeRecord.RecordTimeStamp);
                if (totalOfficeHour > dailyTotalOfficeHour){
                    return dailyTotalOfficeHour;
                } else {
                    return totalOfficeHour;
                }
            }


            return 0;
        }

        public AttendanceRecord GetLastRecord(DateTime dateTime){
            List<AttendanceRecord> attendanceRecords = this.AttendanceRecords.Where(o => o.RecordTimeStamp.Year == dateTime.Year && o.RecordTimeStamp.Month == dateTime.Month && o.RecordTimeStamp.Date == dateTime.Date).OrderBy(i=>i.RecordTimeStamp).ToList();
            if (attendanceRecords != null && attendanceRecords.Count != 0){
                return attendanceRecords.LastOrDefault();
            }

            return null;
        }

		public string GetWarningStatus(DateTime dateTime){


			if (this.IsAbsence(dateTime)) {
				AttendanceRecord TodayStartTimeRecord = this.AttendanceRecords.Where(o => o.RecordTimeStamp.Date == dateTime.Date).ToList().FirstOrDefault();
				if (TodayStartTimeRecord != null){
					return AttendanceStatus.WorkingButAbsenceRecordExist;	
                } else {
                    return string.Empty;
                }
            }

            if (this.CalcuateStatus(dateTime) == AttendanceStatus.NoRecord){
                return AttendanceStatus.NoRecord;
            }

			if (this.OTMin(dateTime) > 0) {
				return AttendanceStatus.OT;
			}


			if (this.IsLate(dateTime)){
				return AttendanceStatus.late;
			}

			return string.Empty;
		}
    }
}
