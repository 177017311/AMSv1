﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.Models.AccountViewModels {
	public class StaffAbsenceListItemViewModel {  
		public string id { get; set; }
		public string leaveType { get; set; }
        public string leaveStartDate { get; set; }
		public string leaveEndDate { get; set; }
        public string remark { get; set; }
        public string status { get; set; }
        public string staffName { get; set; }
		public string staffTitle { get; set; }
    }
}
