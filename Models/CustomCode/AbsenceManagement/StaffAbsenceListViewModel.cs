﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.Models.AccountViewModels {
	public class StaffAbsenceListViewModel {
		public StaffAbsenceListViewModel() {
			staffAbsenceList = new List<StaffAbsenceListItemViewModel>();
			monthList = new List<string>();
			absenceStatusList = new List<string>();
            deptList = new List<string>();
        }

        public string division { get; set; }
        public string department { get; set; }
        public List<string> deptList { get; set; }
        public string selectedDept { get; set; }
		public ICollection<StaffAbsenceListItemViewModel> staffAbsenceList { get; set; }
		public List<string> monthList { get; set; }
		public List<string> absenceStatusList { get; set; }
        public string selectedMonth { get; set; }
		public string selectedAbsenceStatus { get; set; }
    }
}
