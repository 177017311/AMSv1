﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.Models {
	public class AttendanceRecordViewModel {
		public AttendanceRecordViewModel() {
        }

		public int ID { get; set; }
        public string Location { get; set; }
        public string RecordTimeStamp { get; set; }
        public string Status { get; set; }
    }
}
