﻿using System;
namespace AMS.Models {
    public class DailyAttendanceRecord {
        public string TodayWorkTimeLocation { get; set; }
        public DateTime? TodayWorkTime { get; set; }
        public string TodayLeaveTimeLocation { get; set; }
        public DateTime? TodayLeaveTime { get; set; }
        public string Status { get; set; }
		public bool IsLate { get; set; }
		public string AbsenceType { get; set; }

    }
}