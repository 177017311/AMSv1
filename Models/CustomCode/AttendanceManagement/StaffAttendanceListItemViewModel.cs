﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.Models.AccountViewModels {
    public class StaffAttendanceListItemViewModel {
		public string id { get; set; }
        public string StaffName { get; set; }
        public string StaffTitle { get; set; }
        public string TodayStatus { get; set; }
		public string TodayWorkTime { get; set; }
        public string TodayLeaveTime { get; set; }
		public bool  IsLate { get; set; }
		public string AbsenceType { get; set; }
		public string WarningStatus { get; set; }
    }
}
