﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.Models.AccountViewModels {
	public class StaffAttendanceListViewModel {
		public StaffAttendanceListViewModel() {
			staffAttendanceList = new List<StaffAttendanceListItemViewModel>();
			dateList = new List<string>();
            deptList = new List<string>();
        }

        public string division { get; set; }
        public string department { get; set; }
        public List<string> deptList { get; set; }
        public string selectedDept { get; set; }
		public ICollection<StaffAttendanceListItemViewModel> staffAttendanceList { get; set; }
		public List<string> dateList { get; set; }
		public string selectedDate { get; set; }
    }
}
