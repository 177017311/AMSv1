﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.Models.AccountViewModels {
	public class UpdateStaffAttendanceRecordViewModel {
		public UpdateStaffAttendanceRecordViewModel() {
			attendanceRecordViewModels = new List<AttendanceRecordViewModel>();
			hourList = new List<string>();
			minList = new List<string>();
		}
        

		public string id { get; set; }
		public string division { get; set; }
		public string department { get; set; }
		public string staffName { get; set; }
		public string staffTitle { get; set; }
		public ICollection<AttendanceRecordViewModel> attendanceRecordViewModels { get; set; }
		public string selectedDate { get; set; }
		public string dutyHour { get; set; }
		public string dutyMin { get; set; }
		public string offHour { get; set; }
        public string offMin { get; set; }
		public List<string> hourList { get; set; }
		public List<string> minList { get; set; }
	}
}
