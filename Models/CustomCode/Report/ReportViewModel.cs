﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.Models.AccountViewModels {
    public class ReportViewModel {
        public ReportViewModel() {
            deptList = new List<string>();
            MonthList = new List<string>();
            YearList = new List<string>();
        }

        public string division { get; set; }
        public string department { get; set; }

        public List<string> deptList { get; set; }
        public string selectedDept { get; set; }

        public List<string> MonthList { get; set; }
        public string selectedMonth { get; set; }

        public List<string> YearList { get; set; }
        public string selectedYear { get; set; }

        public bool IsMonthly { get; set; }
        public bool IsYearly { get; set; }
        public bool IsUrgentList { get; set; }
    }
}
