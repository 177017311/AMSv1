﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.Models {
    public class StaffAccountItemViewModel {
        public StaffAccountItemViewModel() {
            deptList = new List<string>();
            staffTypeList = new List<string>();
        }

        public string Id { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        //[StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        //[DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string phoneNumber { get; set; }

        [Required]
        [Display(Name = "Job Title")]
        public string Title { get; set; }

        [Required]
        [Display(Name = "Staff Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Account Effective Start Date")]
        public string EffectiveStart { get; set; }

        [Required]
        [Display(Name = "Account Effective End Date")]
        public string EffecticeEnd { get; set; }

        [Display(Name = "Is Manager?")]
        public bool IsManager { get; set; }

        [Display(Name = "Is HR Staff?")]
        public bool IsHRStaff { get; set; }

        [Display(Name = "Is System Admin?")]
        public bool IsAdmin { get; set; }

        public List<string> deptList { get; set; }

        [Required]
        public string selectedDept { get; set; }

        public List<string> staffTypeList { get; set; }

        [Required]
        public string selectedStaffType { get; set; }

    }
}
