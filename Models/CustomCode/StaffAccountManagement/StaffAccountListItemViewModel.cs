﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.Models {
	public class StaffAccountListItemViewModel {
		public StaffAccountListItemViewModel() {
        }

		public string id { get; set; }
        public string staffName { get; set; }
        public string staffTitle { get; set; }
        public string staffType { get; set; }
    }
}
