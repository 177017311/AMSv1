﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AMS.Models {
	public class StaffAccountListViewModel {
		public StaffAccountListViewModel() {
            staffAttendanceList = new List<StaffAccountListItemViewModel>();
			deptList = new List<string>();
			statusList = new List<string>();
        }

        public string division { get; set; }
        public string department { get; set; }
        public ICollection<StaffAccountListItemViewModel> staffAttendanceList { get; set; }
        public List<string> deptList { get; set; }
		public string selectedDept { get; set; }
		public List<string> statusList { get; set; }
		public string selectedStatus { get; set; }
    }
}
