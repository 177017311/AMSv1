﻿using System;
using System.Collections.Generic;

namespace AMS.Models
{
    public class HotspotLocation
    {
        public int ID { get; set; }
        public string LocationCode { get; set; }
        public string LocationDescription { get; set; }
        public string iBeaconLocationMajor { get; set; }
        public string iBeaconLocationMinor { get; set; }
        public ICollection<AttendanceRecord> AttendanceRecords { get; set; }
    }
}
