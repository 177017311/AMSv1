﻿using System;
using System.Collections.Generic;

namespace AMS.Models
{
	public partial class Organization
    {
        public int ID { get; set; }
        public string Division { get; set; }
        public string Department { get; set; }
        public string Section { get; set; }
        public ICollection<ApplicationUser> ApplicationUsers { get; set; }
    }
}
