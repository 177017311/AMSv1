﻿using System;
namespace AMS.Models
{
    public class SystemParameter
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string Value { get; set; }
    }
}
