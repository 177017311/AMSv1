﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace AMS {
	public class Helper {
		public List<DateTime> getAllDates(int year, int month) {
			var ret = new List<DateTime>();
			for (int i = 1; i <= DateTime.DaysInMonth(year, month); i++) {
				ret.Add(new DateTime(year, month, i));
			}
			return ret;
		}

		public List<string> getAllDatesBystring(int year, int month) {
			List<DateTime> allDates = this.getAllDates(year, month);

			List<string> allStringDates = new List<string>();

			foreach (DateTime currentDateTime in allDates) {
				allStringDates.Add(currentDateTime.ToString(common.DateFormat));
			}

			return allStringDates;
		}


		public List<string> getAllMonthBystring() {
         
			List<string> allMonth = new List<string>();

			for (int i = 1; i <= 12; i++) {
				allMonth.Add(i.ToString().PadLeft(2, '0'));
            }

			return allMonth;
        }

        public List<string> getYearListBystring() {

            List<string> Years = new List<string>();
            Years.Add(DateTime.Now.AddYears(-4).Year.ToString());
            Years.Add(DateTime.Now.AddYears(-3).Year.ToString());
            Years.Add(DateTime.Now.AddYears(-2).Year.ToString());
            Years.Add(DateTime.Now.AddYears(-1).Year.ToString());
            Years.Add(DateTime.Now.Year.ToString());

            return Years;
        }

		public List<string> getAllAbsenceStatus(){
			List<string> allAbsenceStatus = new List<string>();

			allAbsenceStatus.Add(AbsenceStatus.created);
			allAbsenceStatus.Add(AbsenceStatus.approved);
			allAbsenceStatus.Add(AbsenceStatus.rejected);

			return allAbsenceStatus;
		}

		public List<string> getAbsenceApprovalStatus() {
			List<string> allAbsenceStatus = new List<string>();
            
			allAbsenceStatus.Add(AbsenceStatus.approved);
			allAbsenceStatus.Add(AbsenceStatus.rejected);

			return allAbsenceStatus;
		}

        public List<string> getStaffAccountStatus() {
            List<string> allStaffAccountStatus = new List<string>();

            allStaffAccountStatus.Add(StaffAccountStatus.Effective);
            allStaffAccountStatus.Add(StaffAccountStatus.Ineffective);

            return allStaffAccountStatus;
        }

        public List<string> getStaffType() {
            List<string> allStaffType = new List<string>();

            allStaffType.Add(StaffType.Staff);
            allStaffType.Add(StaffType.DepartmentManager);
            allStaffType.Add(StaffType.HRStaff);
            allStaffType.Add(StaffType.ITAdmin);

            return allStaffType;
        }
       
		public DateTime stringToDateTime(string date, string hour, string min) {
			return DateTime.ParseExact(date + " " + hour + ":" + min, common.DateTimeFormat, CultureInfo.InvariantCulture);
        }

		public List<string> getHourList(){
			List<string> timeList = new List<string>();
	
			for (int i = 0; i <= 23; i++) {
				timeList.Add(i.ToString().PadLeft(2,'0'));
            }
            
			return timeList;
		}

		public List<string> getMinList() {
            List<string> timeList = new List<string>();

            for (int i = 0; i <= 59; i++) {
                timeList.Add(i.ToString().PadLeft(2, '0'));
            }

            return timeList;
        }

        public string GenerateRandomPassword(int length) {

            string[] randomChars = new[] {
                "ABCDEFGHJKLMNOPQRSTUVWXYZ",    // uppercase 
                "abcdefghijkmnopqrstuvwxyz",    // lowercase
                "0123456789",                   // digits
                "!@$?"                        // non-alphanumeric
            };
            Random rand = new Random(Environment.TickCount);
            List<char> chars = new List<char>();
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[0][rand.Next(0, randomChars[0].Length)]);

   
                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[1][rand.Next(0, randomChars[1].Length)]);


                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[2][rand.Next(0, randomChars[2].Length)]);


                chars.Insert(rand.Next(0, chars.Count),
                    randomChars[3][rand.Next(0, randomChars[3].Length)]);

            for (int i = chars.Count; i < length; i++) {
                string rcs = randomChars[rand.Next(0, randomChars.Length)];
                chars.Insert(rand.Next(0, chars.Count),
                    rcs[rand.Next(0, rcs.Length)]);
            }

            return new string(chars.ToArray());
        }


        public double GetCompareHour(DateTime compare1,DateTime compare2) {
            TimeSpan ts1 = new TimeSpan(compare1.Ticks);
            TimeSpan ts2 = new TimeSpan(compare2.Ticks);
            TimeSpan ts = ts1.Subtract(ts2).Duration();
            return ts.TotalHours;
        }

        public double GetCompareMin(DateTime compare1, DateTime compare2) {
			TimeSpan ts1 = new TimeSpan(compare1.Hour,compare1.Minute,compare1.Second);
			TimeSpan ts2 = new TimeSpan(compare2.Hour, compare2.Minute, compare2.Second);


            TimeSpan ts = ts1.Subtract(ts2).Duration();
			return ts.TotalMinutes;
        }

   	}
}
